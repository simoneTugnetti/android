package it.consoft.provareadpdf.lottie

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import it.consoft.provareadpdf.R
import it.consoft.provareadpdf.databinding.LottieFragmentBinding
import it.consoft.provareadpdf.databinding.LottieFragmentBindingImpl

class LottieFragment : Fragment() {

    private lateinit var viewModel: LottieViewModel
    private lateinit var binding: LottieFragmentBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {

        viewModel = ViewModelProvider(this).get(LottieViewModel::class.java)
        binding = DataBindingUtil.inflate(inflater, R.layout.lottie_fragment, container,
            false)

        binding.viewModel = viewModel
        binding.lifecycleOwner = this

        return binding.root
    }

}