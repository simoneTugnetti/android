package it.consoft.provareadpdf.read

import android.graphics.Bitmap
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class ReadViewModel: ViewModel() {

    private val _readSuccess = MutableLiveData<Bitmap>()
    val readSuccess: LiveData<Bitmap>
    get() = _readSuccess

    fun successPDF(bitmap: Bitmap) {
        _readSuccess.value = bitmap
    }



}