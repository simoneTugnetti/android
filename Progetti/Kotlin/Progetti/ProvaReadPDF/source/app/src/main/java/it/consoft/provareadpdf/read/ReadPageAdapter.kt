package it.consoft.provareadpdf.read

import android.graphics.Bitmap
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import it.consoft.provareadpdf.R
import it.consoft.provareadpdf.utils.loadPDF

class ReadPageAdapter: RecyclerView.Adapter<ReadPageAdapter.ViewHolder>() {

    var data = listOf<Bitmap>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder.from(parent)

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(data[position])

    override fun getItemCount(): Int = data.size

    class ViewHolder private constructor(itemView: View): RecyclerView.ViewHolder(itemView) {
        private val itemImage = itemView.findViewById<ImageView>(R.id.itemPage)

        fun bind(item: Bitmap) = itemImage.loadPDF(item)

        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val view = layoutInflater.inflate(R.layout.item_read_pages, parent,
                        false)
                return ViewHolder(view)
            }
        }

    }

}