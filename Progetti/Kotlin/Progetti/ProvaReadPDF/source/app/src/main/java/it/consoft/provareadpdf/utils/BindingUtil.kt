package it.consoft.provareadpdf.utils

import android.graphics.Bitmap
import android.widget.ImageView
import androidx.databinding.BindingAdapter

@BindingAdapter("loadPDF")
fun ImageView.loadPDF(bitmap: Bitmap?) {
    bitmap?.let {
        setImageBitmap(it)
    }
}