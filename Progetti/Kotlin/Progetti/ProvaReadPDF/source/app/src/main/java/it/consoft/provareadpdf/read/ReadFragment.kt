package it.consoft.provareadpdf.read

import android.content.Context
import android.graphics.Bitmap
import android.graphics.pdf.PdfRenderer
import android.os.Bundle
import android.os.ParcelFileDescriptor
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.net.toUri
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import it.consoft.provareadpdf.R
import it.consoft.provareadpdf.databinding.FragmentReadBinding
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import okhttp3.ResponseBody
import java.io.*

class ReadFragment : Fragment() {

    private lateinit var viewModel: ReadViewModel
    private lateinit var binding: FragmentReadBinding
    private lateinit var file: ParcelFileDescriptor
    private lateinit var pdf: PdfRenderer
    private lateinit var currentPage: PdfRenderer.Page

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle? ): View {

        viewModel = ViewModelProvider(this).get(ReadViewModel::class.java)
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_read, container,
            false)

        binding.viewModel = viewModel
        binding.lifecycleOwner = this

        binding.readPDF.setOnClickListener {
            val file = File(requireContext().filesDir, "sample.pdf")
            GlobalScope.launch(Dispatchers.IO) {
                if (savePdf(file)) {
                    requireActivity().runOnUiThread { readPDF(file) }
                }
            }
        }

        binding.btnLottie.setOnClickListener {
            findNavController().navigate(ReadFragmentDirections.actionReadFragmentToLottieFragment())
        }

        return binding.root
    }

    private fun savePdf(path: File): Boolean {
        try{
            var input: InputStream? = null
            var output: OutputStream? = null
            try {
                val fileReader = byteArrayOf(4096.toByte())
                var read: Int
                input = requireContext().assets.open("sample.pdf")
                output = FileOutputStream(path)

                while (true) {
                    read = input.read(fileReader)
                    if (read == -1) break
                    output.write(fileReader, 0, read)
                }

                output.flush()
                return true
            } catch (e: IOException) {
                return false
            } finally {
                input?.close()
                output?.close()
            }
        } catch (e: IOException) {
            return false
        }
    }

    private fun readPDF(path: File) {
        file = requireActivity().contentResolver.openFileDescriptor(path.toUri(), "r")!!
        pdf = PdfRenderer(file)
        val adapter = ReadPageAdapter()
        val listBitmap = mutableListOf<Bitmap>()
        for (i in 0 until pdf.pageCount) {
            currentPage = pdf.openPage(i)
            val bitmap = Bitmap.createBitmap(binding.listPages.width, binding.listPages.height,
                    Bitmap.Config.ARGB_8888)

            currentPage.render(bitmap, null, null,
                    PdfRenderer.Page.RENDER_MODE_FOR_DISPLAY)

            listBitmap.add(bitmap)
            currentPage.close()
        }
        adapter.data = listBitmap

        binding.listPages.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL,
                false)

        binding.listPages.adapter = adapter

    }

    override fun onDestroy() {
        super.onDestroy()
        file.close()
        pdf.close()
        currentPage.close()
    }

}