package it.consoft.provafotobianconerocamerax

import android.Manifest
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.core.content.ContextCompat
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.single.PermissionListener

class MainFragment : Fragment() {

    private lateinit var btnMakePhoto: Button
    private lateinit var imgPhoto: RecyclerView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btnMakePhoto = view.findViewById(R.id.button)
        imgPhoto = view.findViewById(R.id.list_images)

        btnMakePhoto.setOnClickListener {
            checkCameraPermission()
        }

    }

    override fun onResume() {
        super.onResume()

        val imageBitmap = MainFragmentArgs.fromBundle(requireArguments()).image

        imageBitmap?.let {
            val adapter = PhotoImageAdapter()
            adapter.data = it.toList()
            imgPhoto.adapter = adapter
            val divider = DividerItemDecoration(imgPhoto.context, LinearLayoutManager.HORIZONTAL)
            divider.setDrawable(ContextCompat.getDrawable(requireContext(), R.drawable.divider)!!)
            imgPhoto.addItemDecoration(divider)
        }

    }

    private fun checkCameraPermission() =
        Dexter.withContext(requireContext()).withPermission(Manifest.permission.CAMERA)
            .withListener(object : PermissionListener {
                override fun onPermissionGranted(p0: PermissionGrantedResponse?) {
                    startCamera()
                }

                override fun onPermissionDenied(p0: PermissionDeniedResponse?) {
                    p0?.let {
                        if (it.isPermanentlyDenied) {
                            val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                Uri.fromParts("package", requireActivity().packageName, null))
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                            startActivity(intent)
                        }
                    }
                }

                override fun onPermissionRationaleShouldBeShown(p0: PermissionRequest?,
                                                                p1: PermissionToken?) {
                    p1?.continuePermissionRequest()
                }

            }).check()

    private fun startCamera() =
        findNavController().navigate(MainFragmentDirections.actionMainFragmentToPhotoFragment())

}