package it.consoft.provamqttservice

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.addCallback
import androidx.core.app.NotificationCompat
import androidx.navigation.fragment.findNavController
import it.consoft.provamqttservice.databinding.FragmentConnectBinding
import org.eclipse.paho.client.mqttv3.*

class ConnectFragment : Fragment() {

    private lateinit var binding: FragmentConnectBinding
    private lateinit var notificationManager: NotificationManager
    private lateinit var notify: NotificationCompat.Builder
    private lateinit var pendingIntent: PendingIntent
    private var counterIdNotification = 0

    companion object {
        private const val CHANNEL_ID = "MQTT_NOTIFICATION_CHANNEL"
        private const val CHANNEL_NAME = "MQTT_CHANNEL_MAIN"
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {

        binding = FragmentConnectBinding.inflate(inflater, container, false)

        manageConnect()

        manageBackButton()

        notificationManager = requireActivity().getSystemService(Context.NOTIFICATION_SERVICE)
                as NotificationManager

        return binding.root
    }

    private fun manageConnect() = binding.btnConnect.setOnClickListener {
        MQTTService.setClient(requireContext(), binding.serverUriEdit.text.toString(),
            binding.clientIdEdit.text.toString())

        MQTTService.connect(binding.usernameEdit.text.toString(), binding.passwordEdit.text.toString(),
            object: IMqttActionListener {
                override fun onSuccess(asyncActionToken: IMqttToken?) {
                    findNavController().navigate(R.id.action_connectFragment_to_clientFragment)
                }

                override fun onFailure(asyncActionToken: IMqttToken?, exception: Throwable?) {
                    Log.e(this.javaClass.simpleName, "Fail Connect: ${exception.toString()}")
                    Toast.makeText(requireContext(), "Connection Failure", Toast.LENGTH_SHORT).show()
                }
            }, object: MqttCallback {
                override fun connectionLost(cause: Throwable?) {
                    Log.e(this.javaClass.simpleName, "Connection Lost: ${cause.toString()}")
                    Toast.makeText(requireContext(), "Connection Lost", Toast.LENGTH_SHORT).show()
                }

                override fun messageArrived(topic: String?, message: MqttMessage?) {
                    val msg = "Messaggio ricevuto: ${message.toString()}.\n Topic: $topic"
                    Log.d(this.javaClass.simpleName, msg)

                    val intent = Intent(requireContext(), NotificationActivity::class.java)

                    intent.putExtra("messageIntent", message.toString())

                    pendingIntent = PendingIntent.getActivity(requireContext(), 200, intent,
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) PendingIntent.FLAG_IMMUTABLE
                        else PendingIntent.FLAG_ONE_SHOT)

                    notify = NotificationCompat.Builder(requireContext(), CHANNEL_ID)
                        .setSmallIcon(R.drawable.ic_baseline_account_balance_24)
                        .setContentTitle("Prova MQTT Messeggio Ricevuto")
                        .setContentText(msg)
                        .setDefaults(NotificationCompat.DEFAULT_ALL)
                        .setPriority(NotificationCompat.PRIORITY_HIGH)
                        .setAutoCancel(true)
                        .setContentIntent(pendingIntent)

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
                        notificationManager.createNotificationChannel(NotificationChannel(CHANNEL_ID,
                            CHANNEL_NAME, NotificationManager.IMPORTANCE_HIGH))

                    notificationManager.notify(CHANNEL_ID, counterIdNotification, notify.build())

                    counterIdNotification++

                }

                override fun deliveryComplete(token: IMqttDeliveryToken?) {
                    Toast.makeText(requireContext(), "Messaggio Inviato", Toast.LENGTH_SHORT).show()
                }

            })

    }

    private fun manageBackButton() = requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner) {
        requireActivity().moveTaskToBack(true)
    }

}