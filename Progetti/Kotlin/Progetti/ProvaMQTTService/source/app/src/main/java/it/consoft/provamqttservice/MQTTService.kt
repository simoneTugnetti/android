package it.consoft.provamqttservice

import android.annotation.SuppressLint
import android.content.Context
import org.eclipse.paho.android.service.MqttAndroidClient
import org.eclipse.paho.client.mqttv3.*

object MQTTService {

    @SuppressLint("StaticFieldLeak")
    private lateinit var mqttClient: MqttAndroidClient

    fun setClient(context: Context?, serverUri: String, clientId: String = "") {
        mqttClient = MqttAndroidClient(context, serverUri, clientId)
    }

    fun connect(username: String = "", password: String = "", cbConnect: IMqttActionListener,
                cbClient: MqttCallback) {
        mqttClient.setCallback(cbClient)

        val options = MqttConnectOptions()
        if (username.isNotBlank()) options.userName = username
        options.password = password.toCharArray()

        try {
            mqttClient.connect(options, null, cbConnect)
        } catch (e: MqttException) {
            e.printStackTrace()
        }

    }

    fun subscribe(topic: String, qos: Int = 1, cbSubscribe: IMqttActionListener) {
        try {
            mqttClient.subscribe(topic, qos, null, cbSubscribe)
        } catch (e: MqttException) {
            e.printStackTrace()
        }
    }

    fun unsubscribe(topic: String, cbUnsubscribe: IMqttActionListener) {
        try {
            mqttClient.unsubscribe(topic, null, cbUnsubscribe)
        } catch (e: MqttException) {
            e.printStackTrace()
        }
    }

    fun publish(topic: String, msg: String, qos: Int = 1, retained: Boolean = false,
                cbPublish: IMqttActionListener) {
        try {
            val message = MqttMessage()
            message.payload = msg.toByteArray()
            message.qos = qos
            message.isRetained = retained
            mqttClient.publish(topic, message, null, cbPublish)
        } catch (e: MqttException) {
            e.printStackTrace()
        }

    }

    fun disconnect(cbDisconnect: IMqttActionListener) {
        try {
            mqttClient.disconnect(null, cbDisconnect)
        } catch (e: MqttException) {
            e.printStackTrace()
        }
    }

}