package it.consoft.provamqttservice

import android.content.ActivityNotFoundException
import android.content.ComponentName
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView

class NotificationActivity : AppCompatActivity() {

    private lateinit var title: TextView
    private lateinit var intentButton: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_notification)

        title = findViewById(R.id.title)
        intentButton = findViewById(R.id.goToIrethAuthenticationBtn)

        val text = intent.getStringExtra("messageIntent").toString()

        title.text = "Messaggio ricevuto: $text"

        intentButton.setOnClickListener {
            val cn = ComponentName("it.ireth.authenticator", "it.ireth.authenticator.ui.splash.SplashActivity")
            val intent = Intent()
            intent.component = cn
            val bundle = Bundle()
            bundle.putString("messageReceived", text)
            try {
                startActivity(intent, bundle)
            } catch (e: ActivityNotFoundException) {  }
        }

    }
}