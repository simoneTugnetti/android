package it.consoft.provamqttservice

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.addCallback
import androidx.navigation.fragment.findNavController
import it.consoft.provamqttservice.databinding.FragmentClientBinding
import org.eclipse.paho.client.mqttv3.IMqttActionListener
import org.eclipse.paho.client.mqttv3.IMqttToken

class ClientFragment : Fragment() {

    private lateinit var binding: FragmentClientBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {

        binding = FragmentClientBinding.inflate(inflater, container, false)

        managePublish()

        manageSubscribe()

        manageUnsubscribe()

        manageDisconnect()

        // manageBackButton()

        return binding.root
    }

    private fun managePublish() = binding.btnPublish.setOnClickListener {
        MQTTService.publish(topic = binding.topicPubEdit.text.toString(),
            msg = binding.messageEdit.text.toString(), cbPublish = object: IMqttActionListener {
                override fun onSuccess(asyncActionToken: IMqttToken?) {
                    val msg = "Messaggio pubblicato: ${binding.messageEdit.text}\n Topic: ${binding.topicPubEdit.text}"
                    Log.d(this.javaClass.simpleName, msg)
                    Toast.makeText(requireContext(), msg, Toast.LENGTH_SHORT).show()
                }

                override fun onFailure(asyncActionToken: IMqttToken?, exception: Throwable?) {
                    Log.e(this.javaClass.simpleName, "Pubblicazione del messaggio fallita")
                }
            })
    }

    private fun manageSubscribe() = binding.btnSubscribe.setOnClickListener {
        MQTTService.subscribe(topic = binding.topicSubEdit.text.toString(),
            cbSubscribe = object: IMqttActionListener {
                override fun onSuccess(asyncActionToken: IMqttToken?) {
                    val msg = "Subscribed to: ${binding.topicSubEdit.text}"
                    Log.d(this.javaClass.simpleName, msg)

                    Toast.makeText(requireContext(), msg, Toast.LENGTH_SHORT).show()
                }

                override fun onFailure(asyncActionToken: IMqttToken?, exception: Throwable?) {
                    Log.e(this.javaClass.simpleName, "Subscribe fail: ${binding.topicSubEdit.text}")
                }
            })
    }

    private fun manageUnsubscribe() = binding.btnUnsubscribe.setOnClickListener {
        MQTTService.unsubscribe(binding.topicSubEdit.text.toString(), object: IMqttActionListener {
            override fun onSuccess(asyncActionToken: IMqttToken?) {
                val msg = "Unsubscribed to: ${binding.topicSubEdit.text}"
                Log.d(this.javaClass.simpleName, msg)

                Toast.makeText(requireContext(), msg, Toast.LENGTH_SHORT).show()
            }

            override fun onFailure(asyncActionToken: IMqttToken?, exception: Throwable?) {
                Log.e(this.javaClass.simpleName, "Unsubscribe fail: ${binding.topicSubEdit.text}")
            }
        })
    }

    private fun manageDisconnect() = binding.btnDisconnect.setOnClickListener {
        MQTTService.disconnect(object: IMqttActionListener {
            override fun onSuccess(asyncActionToken: IMqttToken?) {
                Log.d(this.javaClass.simpleName, "Disconnected")
                Toast.makeText(requireContext(), "MQTT Disconnected", Toast.LENGTH_SHORT).show()

                findNavController().popBackStack()
            }

            override fun onFailure(asyncActionToken: IMqttToken?, exception: Throwable?) {
                Log.e(this.javaClass.simpleName, "Disconnessione fallita")
            }

        })
    }

//    private fun manageBackButton() = requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner) {
//        MQTTService.disconnect(object: IMqttActionListener {
//            override fun onSuccess(asyncActionToken: IMqttToken?) {
//                Log.d(this.javaClass.simpleName, "Disconnected")
//                Toast.makeText(requireContext(), "MQTT Disconnected", Toast.LENGTH_SHORT).show()
//
//                findNavController().popBackStack()
//            }
//
//            override fun onFailure(asyncActionToken: IMqttToken?, exception: Throwable?) {
//                Log.e(this.javaClass.simpleName, "Disconnessione fallita")
//            }
//
//        })
//    }

}