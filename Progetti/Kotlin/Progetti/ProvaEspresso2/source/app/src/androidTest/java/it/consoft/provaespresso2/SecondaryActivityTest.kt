package it.consoft.provaespresso2

import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.rules.ActivityScenarioRule
import org.junit.Assert.*
import org.junit.Rule
import org.junit.Test

class SecondaryActivityTest {

    @get:Rule
    val activityRule = ActivityScenarioRule(SecondaryActivity::class.java)

    @Test
    fun testIsActivityInView() {
        Espresso.onView(withId(R.id.secondary)).check(matches(isDisplayed()))
    }

    @Test
    fun testVisibilityTitleNextButton() {
        Espresso.onView(withId(R.id.activity_secondary_title)).check(matches(isDisplayed()))

        Espresso.onView(withId(R.id.button_back)).check(matches(isDisplayed()))
    }

    @Test
    fun testIsTitleTextDisplayed() {
        Espresso.onView(withId(R.id.activity_secondary_title))
                .check(matches(ViewMatchers.withText(R.string.text_secondaryactivity)))
    }




}