package it.consoft.provaespresso2

import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.pressBack
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import org.junit.Test

class MainActivityTestNav {

    @Test
    fun testNavInSecondaryActivity() {
        val activityScenario = ActivityScenario.launch(MainActivity::class.java)

        Espresso.onView(withId(R.id.button_next_activity)).perform(click())
        Espresso.onView(withId(R.id.secondary)).check(matches(isDisplayed()))
    }

    @Test
    fun testBackPressedToMainActivity() {
        val activityScenario = ActivityScenario.launch(MainActivity::class.java)

        onView(withId(R.id.button_next_activity)).perform(click())
        onView(withId(R.id.secondary)).check(matches(isDisplayed()))

        // method 1
        // onView(withId(R.id.button_back)).perform(click())

        // method 2
        pressBack()

        onView(withId(R.id.main)).check(matches(isDisplayed()))


    }

}