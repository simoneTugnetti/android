package com.ovolab.provaretrofit.model

data class Schedule(
    val days: List<String>,
    val time: String
)