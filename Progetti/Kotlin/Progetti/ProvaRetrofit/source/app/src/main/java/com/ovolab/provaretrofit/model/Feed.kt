package com.ovolab.provaretrofit.model

import com.google.gson.annotations.SerializedName

/**
 * @author Simone Tugnetti
 */
data class Feed(
    val score: Double,
    @SerializedName("show")
    val show: Show
)