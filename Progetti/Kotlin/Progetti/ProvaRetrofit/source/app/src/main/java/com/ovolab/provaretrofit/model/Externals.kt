package com.ovolab.provaretrofit.model

data class Externals(
    val imdb: String,
    val thetvdb: Int,
    val tvrage: Int
)