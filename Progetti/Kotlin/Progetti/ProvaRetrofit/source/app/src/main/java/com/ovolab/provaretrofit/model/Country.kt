package com.ovolab.provaretrofit.model

data class Country(
    val code: String,
    val name: String,
    val timezone: String
)