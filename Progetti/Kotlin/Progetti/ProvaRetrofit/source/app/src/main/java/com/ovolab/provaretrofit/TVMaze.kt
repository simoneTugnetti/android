package com.ovolab.provaretrofit

import com.ovolab.provaretrofit.model.Feed
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * @author Simone Tugnetti
 */
interface TVMaze {

    @GET("shows")
    fun getFilms(
        @Query("q") genre: String
    ): Call<List<Feed>>

}