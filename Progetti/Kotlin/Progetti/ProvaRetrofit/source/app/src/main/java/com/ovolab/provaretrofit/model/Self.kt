package com.ovolab.provaretrofit.model

data class Self(
    val href: String
)