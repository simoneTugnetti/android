package com.ovolab.provaretrofit.model

data class Rating(
    val average: Double
)