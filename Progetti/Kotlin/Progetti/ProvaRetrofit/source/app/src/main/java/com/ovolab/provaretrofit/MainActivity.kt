package com.ovolab.provaretrofit

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.ovolab.provaretrofit.model.Feed
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * @author Simone Tugnetti
 * Prima prova di utilizzo Retrofit
 */
class MainActivity : AppCompatActivity() {

    // http://api.tvmaze.com/search/shows?q=girls

    private val BASE_URL = "https://api.tvmaze.com/search/"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnText.setOnClickListener {
            retrofitParse()
        }

    }

    private fun retrofitParse() {
        val retrofit = Retrofit.Builder().baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        val tvMaze = retrofit.create(TVMaze::class.java)

        val call: Call<List<Feed>> = tvMaze.getFilms("girls")

        call.enqueue(object: Callback<List<Feed>> {
            override fun onFailure(call: Call<List<Feed>>, t: Throwable) {
                Toast.makeText(applicationContext, t.message, Toast.LENGTH_LONG).show()
            }

            override fun onResponse(call: Call<List<Feed>>, response: Response<List<Feed>>) {
                val films: List<Feed>? = response.body()

                if (films != null) {
                    for (film in films) {
                        Log.i("json", film.show.id.toString())
                    }
                }

            }

        })
    }
}
