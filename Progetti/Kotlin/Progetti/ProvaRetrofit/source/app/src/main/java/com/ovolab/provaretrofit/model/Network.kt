package com.ovolab.provaretrofit.model

data class Network(
    val country: Country,
    val id: Int,
    val name: String
)