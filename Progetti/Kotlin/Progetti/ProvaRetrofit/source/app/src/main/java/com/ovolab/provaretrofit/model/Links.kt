package com.ovolab.provaretrofit.model

data class Links(
    val previousepisode: Previousepisode,
    val self: Self
)