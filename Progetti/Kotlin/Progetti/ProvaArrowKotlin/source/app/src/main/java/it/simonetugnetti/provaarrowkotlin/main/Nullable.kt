package it.simonetugnetti.provaarrowkotlin.main

import arrow.core.continuations.nullable

private fun takeFoodFromRefrigerator(): Lettuce? = null
private fun getKnife(): Knife? = null
private fun prepare(tool: Knife, ingredient: Lettuce): Salad = Salad

suspend fun prepareLunch() = nullable {
    prepare(getKnife().bind(), takeFoodFromRefrigerator().bind()).bind()
}