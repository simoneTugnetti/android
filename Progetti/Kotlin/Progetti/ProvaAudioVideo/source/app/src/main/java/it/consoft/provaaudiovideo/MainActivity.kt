package it.consoft.provaaudiovideo

import android.media.AudioAttributes
import android.media.MediaPlayer
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.MediaController
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val videoSrc = "https://videocdn.bodybuilding.com/video/mp4/62000/62792m.mp4"
    private val audioSrc = "https://www.soundhelix.com/examples/mp3/SoundHelix-Song-1.mp3"
    private lateinit var mediaPlayer: MediaPlayer

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setMediaController()

        prepareAudio()

        startAudio()

    }

    // Must be progressive download
    private fun prepareAudioStreaming() {
        mediaPlayer = MediaPlayer().apply {
            setAudioAttributes(AudioAttributes.Builder()
                    .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                    .setUsage(AudioAttributes.USAGE_MEDIA)
                    .build())
            setDataSource(audioSrc)
            prepareAsync()
        }

    }

    private fun prepareAudio() {
        mediaPlayer = MediaPlayer.create(this, R.raw.sound_helix)
    }

    private fun startAudioStreaming() {
        media_audio.setOnClickListener {
            media_player.pause()
            audio_buffer_text.visibility = View.VISIBLE
            mediaPlayer.setOnPreparedListener {
                audio_buffer_text.visibility = View.GONE
                if (it.isPlaying) it.pause() else it.start()
            }
        }
    }

    private fun startAudio() {
        media_audio.setOnClickListener {
            if (mediaPlayer.isPlaying) {
                mediaPlayer.pause()
                media_player.start()
            } else {
                media_player.pause()
                mediaPlayer.start()
            }
        }
    }

    private fun setMediaController() {
        val mediaController = MediaController(this)

        mediaController.setMediaPlayer(media_player)

        media_player.setMediaController(mediaController)
    }

    private fun initializePlayer() {
        video_buffer_text.visibility = View.VISIBLE

        media_player.setVideoURI(Uri.parse(videoSrc))

        media_player.setOnPreparedListener {
            video_buffer_text.visibility = View.GONE
            it.start()
        }

        media_player.setOnCompletionListener {
            it.seekTo(1)
        }

        media_player.setOnErrorListener { _, _, _ ->
            Toast.makeText(this, "Errore caricamento video", Toast.LENGTH_SHORT).show()
            true
        }

    }

    override fun onStart() {
        super.onStart()
        initializePlayer()
    }

    override fun onPause() {
        super.onPause()
        media_player.pause()
    }

    override fun onStop() {
        super.onStop()
        media_player.stopPlayback()
        mediaPlayer.release()
    }
}