object Config {

    const val MIN_SDK = 26
    const val TARGET_SDK = 35
    const val COMPILE_SDK = 35
    const val VERSION_CODE = 1
    const val VERSION_NAME = "1.0"
    const val NAMESPACE = "it.simonetugnetti.provagradle"
    const val APP_ID = "it.simonetugnetti.provagradle"
    const val JAVA = 21

}
