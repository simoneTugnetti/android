plugins {
    id(libs.plugins.android.application.get().pluginId)
    id(libs.plugins.kotlin.android.get().pluginId)
    alias(libs.plugins.kotlin.compose)
    alias(libs.plugins.kotlin.ksp)
}

android {
    namespace = Config.NAMESPACE
    compileSdk = Config.COMPILE_SDK

    defaultConfig {
        applicationId = Config.APP_ID
        minSdk = Config.MIN_SDK
        targetSdk = Config.TARGET_SDK
        versionCode = Config.VERSION_CODE
        versionName = Config.VERSION_NAME

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    flavorDimensions += listOf("paid_status")
    productFlavors {
        create("free") {
            applicationIdSuffix = ".free"
            dimension = "paid_status"
        }
        create("paid") {
            applicationIdSuffix = ".paid"
            dimension = "paid_status"
        }
    }

    buildTypes {
        debug {
            isMinifyEnabled = false
            buildConfigField("String", "TEST", "\"Giovanni\"")
        }
        create("pqr") {
            isMinifyEnabled = true
            buildConfigField("String", "TEST", "\"Paolo\"")
        }
        release {
            isMinifyEnabled = true
            buildConfigField("String", "TEST", "\"Paolo\"")
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }
    kotlin {
        jvmToolchain(Config.JAVA)
    }
    buildFeatures {
        compose = true
        buildConfig = true
    }
}

dependencies {

    data()

    implementation(libs.androidx.core.ktx)
    implementation(libs.androidx.lifecycle.runtime.ktx)
    implementation(libs.androidx.activity.compose)
    implementation(platform(libs.androidx.compose.bom))
    implementation(libs.androidx.ui)
    implementation(libs.androidx.ui.graphics)
    implementation(libs.androidx.ui.tooling.preview)
    implementation(libs.androidx.material3)

    // implementation(Dependencies.RETROFIT)
    room()
    retrofit()

    testImplementation(libs.junit)
    androidTestImplementation(libs.androidx.junit)
    androidTestImplementation(libs.androidx.espresso.core)
    androidTestImplementation(platform(libs.androidx.compose.bom))
    androidTestImplementation(libs.androidx.ui.test.junit4)
    debugImplementation(libs.androidx.ui.tooling)
    debugImplementation(libs.androidx.ui.test.manifest)
}