package it.consoft.provagooglelocation

import android.Manifest
import android.content.Context
import android.content.Intent
import android.location.LocationManager
import android.os.Bundle
import android.provider.Settings
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.location.*
import com.google.android.gms.tasks.CancellationToken
import com.google.android.gms.tasks.OnTokenCanceledListener
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.single.PermissionListener
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    private lateinit var locationManager: LocationManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager

        googleBtn.setOnClickListener {
            if (checkGPS()) checkPermissionPosition()
            else turnOnGPS()
        }

    }

    private fun checkPermissionPosition() =
        Dexter.withContext(this).withPermission(Manifest.permission.ACCESS_FINE_LOCATION)
            .withListener(object : PermissionListener {
                override fun onPermissionGranted(p0: PermissionGrantedResponse?) {
                    checkLocation()
                }

                override fun onPermissionDenied(p0: PermissionDeniedResponse?) {
                    Toast.makeText(this@MainActivity, "Permesso negato", Toast.LENGTH_SHORT).show()
                }

                override fun onPermissionRationaleShouldBeShown(p0: PermissionRequest?,
                                                                p1: PermissionToken?) {
                    p1?.continuePermissionRequest()
                }

        }).check()

    private fun turnOnGPS() = startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))

    private fun checkGPS() = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)

    fun checkLocation() {
        val mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(applicationContext)

        try {
            val location = mFusedLocationProviderClient.getCurrentLocation(LocationRequest.PRIORITY_HIGH_ACCURACY,
                object: CancellationToken() {
                    override fun isCancellationRequested(): Boolean {
                        return isCancellationRequested
                    }

                    override fun onCanceledRequested(p0: OnTokenCanceledListener): CancellationToken {
                        return this
                    }
                })
            location.addOnSuccessListener {
                resultTxt.text = "Latitudine: ${it.latitude} \n Longitudine: ${it.longitude}"
            }
        } catch (e: SecurityException) {  }

    }

}