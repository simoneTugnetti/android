package it.consoft.provaauthsystempin

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import it.consoft.provaauthsystempin.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnKeyGuard.setOnClickListener {
            startActivity(Intent(this, KeyguardManagerActivity::class.java))
        }

        binding.btnBiometric.setOnClickListener {
            startActivity(Intent(this, BiometricActivity::class.java))
        }


    }

}