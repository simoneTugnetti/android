package it.consoft.provaauthsystempin

import android.app.KeyguardManager
import android.content.Intent
import android.os.Bundle
import android.widget.TextView
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import it.consoft.provaauthsystempin.databinding.ActivityKeyguardManagerBinding

class KeyguardManagerActivity : AppCompatActivity() {

    private lateinit var binding: ActivityKeyguardManagerBinding
    private lateinit var registerKeyAuth: ActivityResultLauncher<Intent>
    private lateinit var title: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityKeyguardManagerBinding.inflate(layoutInflater)
        setContentView(binding.root)

        title = binding.titleKeyGuard

        registerKeyAuthIntent()

        val km = getSystemService(KEYGUARD_SERVICE) as KeyguardManager

        if (km.isKeyguardSecure) {
            val promptInfo = km.createConfirmDeviceCredentialIntent("Autenticazione",
                "Procedi con l'autenticazione")
            registerKeyAuth.launch(promptInfo)
        } else title.text = "Nessuna autenticazione"

    }

    private fun registerKeyAuthIntent() {
        registerKeyAuth = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
            if (it.resultCode == RESULT_OK)
                title.text = "Autenticazione effettuata"
            else title.text = "Autenticazione fallita"
        }
    }

}