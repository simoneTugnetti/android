package it.consoft.provaauthsystempin

import androidx.biometric.BiometricPrompt

interface BiometricAuthListener {
    fun onBiometricAuthError(errorCode: Int, errorString: CharSequence)
    fun onBiometricAuthSuccess(result: BiometricPrompt.AuthenticationResult)
}