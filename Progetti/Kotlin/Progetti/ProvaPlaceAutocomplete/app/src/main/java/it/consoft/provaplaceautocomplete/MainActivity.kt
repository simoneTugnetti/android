package it.consoft.provaplaceautocomplete

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.google.android.gms.common.api.Status
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.AutocompleteActivity
import com.google.android.libraries.places.widget.AutocompleteSupportFragment
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val AUTOCOMPLETE_ID = 200

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        Places.initialize(this, getString(R.string.place_api_key))

        val autoComplete = autocomplete_container as AutocompleteSupportFragment

        val field = listOf(Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG)

        autoComplete.setPlaceFields(field)

        autoComplete.setCountries("IT")

        autoComplete.setOnPlaceSelectedListener(object: PlaceSelectionListener {
            override fun onPlaceSelected(p0: Place) {
                textView.text = "Lat: ${p0.latLng?.latitude ?: 0}, Long: ${p0.latLng?.longitude ?: 0}"
            }

            override fun onError(p0: Status) {
                textView.text = "Error"
                Log.e("MAIN", "Error: ${p0.statusMessage}")
            }

        })

        val intent = Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, field).build(this)

        button.setOnClickListener {
            startActivityForResult(intent, AUTOCOMPLETE_ID)
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == AUTOCOMPLETE_ID) {
            when (resultCode) {
                Activity.RESULT_OK -> {
                    data?.let {
                        val place = Autocomplete.getPlaceFromIntent(data)
                        textView.text = "Lat: ${place.latLng?.latitude ?: 0}, Long: ${place.latLng?.longitude ?: 0}"
                    }
                }
                AutocompleteActivity.RESULT_ERROR -> {
                    data?.let {
                        val status = Autocomplete.getStatusFromIntent(data)
                        textView.text = "Error"
                        Log.e("MAIN", "Error: ${status.statusMessage}")
                    }
                }
            }
        }

    }

}