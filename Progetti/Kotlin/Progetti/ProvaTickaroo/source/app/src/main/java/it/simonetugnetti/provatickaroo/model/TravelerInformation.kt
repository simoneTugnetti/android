package it.simonetugnetti.provatickaroo.model

import com.tickaroo.tikxml.annotation.PropertyElement
import com.tickaroo.tikxml.annotation.Xml
import it.simonetugnetti.provatickaroo.converter.MyDateConverter
import java.util.Date

@Xml(name = "Travelerinformation")
data class TravelerInformation(
    @PropertyElement val id: Int,
    @PropertyElement val name: String,
    @PropertyElement val email: String,
    @PropertyElement(name = "adderes") val address: String,
    @PropertyElement(name = "createdat", converter = MyDateConverter::class) val createDate: Date
)
