package it.simonetugnetti.provatickaroo.utils

import android.content.Context
import android.content.DialogInterface
import androidx.core.content.ContextCompat
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import it.simonetugnetti.provatickaroo.R

object Utils {

    fun createAlert(context: Context, neutral: DialogInterface.OnClickListener) =
        MaterialAlertDialogBuilder(context).setTitle(context.getString(R.string.title_alert))
            .setIcon(ContextCompat.getDrawable(context, R.drawable.ic_baseline_report_problem))
            .setMessage(context.getString(R.string.message_alert))
            .setNeutralButton(context.getString(R.string.close), neutral)
            .setCancelable(false)
            .create().show()

}