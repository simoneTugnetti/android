package it.simonetugnetti.provatickaroo.model

import com.tickaroo.tikxml.annotation.PropertyElement
import com.tickaroo.tikxml.annotation.Xml

@Xml(name = "Travelerinformation")
data class TravelerRequest(
    @PropertyElement val name: String,
    @PropertyElement val email: String,
    @PropertyElement(name = "adderes") val address: String
)
