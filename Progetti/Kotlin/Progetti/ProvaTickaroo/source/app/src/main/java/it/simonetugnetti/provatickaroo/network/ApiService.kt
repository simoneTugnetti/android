package it.simonetugnetti.provatickaroo.network

import it.simonetugnetti.provatickaroo.model.TravelerInformationResponse
import it.simonetugnetti.provatickaroo.model.TravelerPostResponse
import it.simonetugnetti.provatickaroo.model.TravelerRequest
import retrofit2.Call
import retrofit2.http.*

interface ApiService {

    @POST("Traveler")
    fun getTravelerInfo(@Body request: TravelerRequest): Call<TravelerPostResponse>

    @GET("Traveler")
    fun getTravelersPageInfo(@Query(value = "page") page: Int): Call<TravelerInformationResponse>

}