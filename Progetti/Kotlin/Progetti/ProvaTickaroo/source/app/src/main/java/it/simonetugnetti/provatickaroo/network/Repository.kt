package it.simonetugnetti.provatickaroo.network

import it.simonetugnetti.provatickaroo.model.TravelerInformationResponse
import it.simonetugnetti.provatickaroo.model.TravelerPostResponse
import it.simonetugnetti.provatickaroo.model.TravelerRequest
import it.simonetugnetti.provatickaroo.network.NetworkApi.service
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

object Repository {

    fun getTravelerPostInfo(name: String, email: String, address: String, success: (info: TravelerPostResponse) -> Unit,
                            failure: (t: Throwable) -> Unit) =
        service.getTravelerInfo(TravelerRequest(name, email, address)).enqueue(object: Callback<TravelerPostResponse> {
            override fun onResponse(call: Call<TravelerPostResponse>,
                                    response: Response<TravelerPostResponse>) {
                response.run {
                    if (isSuccessful && body() != null) body()?.let { success.invoke(it) }
                    else failure.invoke(Exception("Response failure or body null"))
                }
            }

            override fun onFailure(call: Call<TravelerPostResponse>, t: Throwable) {
                failure.invoke(t)
            }

        })

    fun getListTravelerInfo(page: Int, success: (info: TravelerInformationResponse) -> Unit,
                            failure: (t: Throwable) -> Unit) =
        service.getTravelersPageInfo(page).enqueue(object: Callback<TravelerInformationResponse> {
            override fun onResponse(call: Call<TravelerInformationResponse>,
                                    response: Response<TravelerInformationResponse>) {
                response.run {
                    if (isSuccessful && body() != null) body()?.let { success.invoke(it) }
                    else failure.invoke(Exception("Response failure or body null"))
                }
            }

            override fun onFailure(call: Call<TravelerInformationResponse>, t: Throwable) {
                failure.invoke(t)
            }

        })

}