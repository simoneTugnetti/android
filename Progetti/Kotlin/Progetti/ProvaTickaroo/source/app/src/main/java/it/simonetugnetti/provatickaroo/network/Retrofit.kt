package it.simonetugnetti.provatickaroo.network

import com.tickaroo.tikxml.TikXml
import com.tickaroo.tikxml.retrofit.TikXmlConverterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit

object NetworkApi {

    private val retrofit = Retrofit.Builder().baseUrl("http://restapi.adequateshop.com/api/")
        .client(getOkHttpClient()).addConverterFactory(TikXmlConverterFactory.create(
            TikXml.Builder().exceptionOnUnreadXml(false).build()
        )).build()

    private fun getOkHttpClient() = OkHttpClient.Builder().addInterceptor(HttpLoggingInterceptor()
        .setLevel(HttpLoggingInterceptor.Level.BODY)).build()

    val service: ApiService = retrofit.create(ApiService::class.java)

}