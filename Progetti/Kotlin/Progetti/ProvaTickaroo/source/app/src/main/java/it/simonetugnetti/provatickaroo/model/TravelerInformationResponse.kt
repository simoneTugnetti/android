package it.simonetugnetti.provatickaroo.model

import com.tickaroo.tikxml.annotation.Element
import com.tickaroo.tikxml.annotation.Path
import com.tickaroo.tikxml.annotation.PropertyElement
import com.tickaroo.tikxml.annotation.Xml

@Xml(name = "TravelerinformationResponse")
data class TravelerInformationResponse(
    @PropertyElement val page: Int,
    @PropertyElement(name = "per_page") val perPage: Int,
    @PropertyElement(name = "totalrecord") val totalRecord: Int,
    @PropertyElement(name = "total_pages") val totalPages: Int,
    @Path("travelers") @Element val travelers: List<TravelerInformation>
)
