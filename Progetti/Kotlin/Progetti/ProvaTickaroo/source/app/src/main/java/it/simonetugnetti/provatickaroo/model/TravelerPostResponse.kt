package it.simonetugnetti.provatickaroo.model

import com.tickaroo.tikxml.annotation.PropertyElement
import com.tickaroo.tikxml.annotation.Xml
import it.simonetugnetti.provatickaroo.converter.MyDateConverter
import java.util.*

@Xml(name = "Travelerinformation")
data class TravelerPostResponse(
    @PropertyElement val id: Int,
    @PropertyElement val name: String,
    @PropertyElement(name = "adderes") val address: String,
    @PropertyElement(name = "createdat", converter = MyDateConverter::class) val date: Date
)
