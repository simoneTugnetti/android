package it.simonetugnetti.provatickaroo.utils

import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView

@BindingAdapter("app:myCustomAdapter")
fun RecyclerView.myCustomAdapter(adapter: RecyclerView.Adapter<*>) {
    this.adapter = adapter
}
