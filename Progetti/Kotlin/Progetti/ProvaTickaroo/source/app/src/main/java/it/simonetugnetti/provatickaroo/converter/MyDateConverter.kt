package it.simonetugnetti.provatickaroo.converter

import com.tickaroo.tikxml.TypeConverter
import java.text.SimpleDateFormat
import java.util.*

class MyDateConverter: TypeConverter<Date> {

    private val formatter = SimpleDateFormat("dd-MM-yyyy", Locale.US)

    override fun read(value: String?) = value?.let { formatter.parse(it) } ?: Date()

    override fun write(value: Date?) = value?.let { formatter.format(it) } ?: ""

}