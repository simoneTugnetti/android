package it.simonetugnetti.provatickaroo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import it.simonetugnetti.provatickaroo.databinding.ActivityMainBinding
import it.simonetugnetti.provatickaroo.utils.Utils
import timber.log.Timber

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    private val viewModel by viewModels<MainViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        binding.lifecycleOwner = this
        binding.viewModel = viewModel

        observeError()

    }

    private fun observeError() = viewModel.travelerError.observe(this) {
        Timber.e(it)
        Utils.createAlert(this) { _, _ ->
            finish()
        }
    }


}