package it.simonetugnetti.provatickaroo

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import it.simonetugnetti.provatickaroo.databinding.ItemListTravelersBinding
import it.simonetugnetti.provatickaroo.model.TravelerInformation
import it.simonetugnetti.provatickaroo.TravelersAdapter.ViewHolder

class TravelersAdapter: ListAdapter<TravelerInformation, ViewHolder>(diffCallback) {

    companion object {
        private val diffCallback = object: DiffUtil.ItemCallback<TravelerInformation>() {
            override fun areItemsTheSame(oldItem: TravelerInformation, newItem: TravelerInformation) =
                oldItem.id == newItem.id

            override fun areContentsTheSame(oldItem: TravelerInformation, newItem: TravelerInformation) =
                oldItem == newItem

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ViewHolder(ItemListTravelersBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(getItem(position))

    inner class ViewHolder(private val binding: ItemListTravelersBinding): RecyclerView.ViewHolder(binding.root) {

        fun bind(item: TravelerInformation) {
            binding.itemListId.text = item.id.toString()
            binding.itemListName.text = item.name
            binding.itemListEmail.text = item.email
        }

    }

}