package it.simonetugnetti.provatickaroo

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import it.simonetugnetti.provatickaroo.network.Repository
import it.simonetugnetti.provatickaroo.utils.Utils
import timber.log.Timber

class MainViewModel: ViewModel() {

    private val _listTravelersAdapter = MutableLiveData<TravelersAdapter>()
    val listTravelersAdapter: LiveData<TravelersAdapter>
    get() = _listTravelersAdapter

    private val _travelerPage = MutableLiveData<Int>()
    val travelerPage: LiveData<Int>
        get() = _travelerPage

    private val _travelerTotalResult = MutableLiveData<Int>()
    val travelerTotalResult: LiveData<Int>
        get() = _travelerTotalResult

    private val _travelerPerPage = MutableLiveData<Int>()
    val travelerPerPage: LiveData<Int>
        get() = _travelerPerPage

    private val _travelerTotalPages = MutableLiveData<Int>()
    val travelerTotalPages: LiveData<Int>
        get() = _travelerTotalPages

    private val _travelerError = MutableLiveData<Throwable>()
    val travelerError: LiveData<Throwable>
        get() = _travelerError

    init {
        _listTravelersAdapter.value = TravelersAdapter()
        _travelerPage.value = 0
        _travelerPerPage.value = 0
        _travelerTotalResult.value = 0
        _travelerTotalPages.value = 0

        getListTravelerInfo(1)

    }

    private fun getListTravelerInfo(page: Int) = Repository.getListTravelerInfo(page,
        {
            _listTravelersAdapter.value?.submitList(it.travelers)
            _travelerPage.value = it.page
            _travelerPerPage.value = it.perPage
            _travelerTotalPages.value = it.totalPages
            _travelerTotalResult.value = it.totalRecord
        }, {
            _travelerError.value = it
        })

}