package it.consoft.provaworddoc

import android.content.ActivityNotFoundException
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.FileProvider
import kotlinx.android.synthetic.main.activity_main.*
import java.io.*


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val path = File(filesDir, "documents")
        if (!path.exists()) path.mkdirs()

        savePdf(File(path, "sample-doc.doc"), assets.open("sample-doc.doc"))
        savePdf(File(path, "sample-docx.docx"), assets.open("sample-docx.docx"))

        button.setOnClickListener {
            val intent = Intent()
            intent.action = Intent.ACTION_VIEW
            val type = "application/msword"
            val photoURI = FileProvider.getUriForFile(this,
                applicationContext.packageName.toString() + ".provider",
                File(path, "sample-doc.doc"))
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            intent.setDataAndType(photoURI, type)
            try {
                startActivity(Intent.createChooser(intent, "Choose an Application:"))
            } catch (e: ActivityNotFoundException) {
                Toast.makeText(this, "Non sono presenti applicazioni per aprire tale documento", Toast.LENGTH_SHORT).show()
            }
        }

        button2.setOnClickListener {
            val intent = Intent()
            intent.action = Intent.ACTION_VIEW
            val type = "application/msword"
            val photoURI = FileProvider.getUriForFile(this,
                applicationContext.packageName.toString() + ".provider",
                File(path, "sample-docx.docx"))
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            intent.setDataAndType(photoURI, type)
            try {
                startActivity(Intent.createChooser(intent, "Choose an Application:"))
            } catch (e: ActivityNotFoundException) {
                Toast.makeText(this, "Non sono presenti applicazioni per aprire tale documento", Toast.LENGTH_SHORT).show()
            }
        }

    }

    private fun savePdf(path: File, body: InputStream): Boolean {
        try{
            var input: InputStream? = null
            var output: OutputStream? = null
            try {
                val fileReader = byteArrayOf(4096.toByte())
                var read: Int
                input = body
                output = FileOutputStream(path)

                while (true) {
                    read = input.read(fileReader)
                    if (read == -1) break
                    output.write(fileReader, 0, read)
                }

                output.flush()
                return true
            } catch (e: IOException) {
                return false
                // createErrorAlert { _, _  -> requireActivity().finish() }
            } finally {
                input?.close()
                output?.close()
                body.close()
            }
        } catch (e: IOException) {
            return false
            // createErrorAlert { _, _  -> requireActivity().finish() }
        }
    }

}