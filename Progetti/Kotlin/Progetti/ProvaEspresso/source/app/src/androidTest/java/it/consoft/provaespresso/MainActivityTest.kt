package it.consoft.provaespresso

import androidx.test.espresso.Espresso
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.typeText
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.rules.ActivityScenarioRule
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class MainActivityTest {

    @get:Rule
    val mActivityTestRule = ActivityScenarioRule(MainActivity::class.java)

    private val name = "Simone"

    @Before
    fun setUp() {
    }

    @Test
    fun userInputScenario() {
        Espresso.onView(withId(R.id.editTextTextPersonName)).perform(typeText(name))
        Espresso.closeSoftKeyboard()
        Espresso.onView(withId(R.id.button)).perform(click())
        Espresso.onView(withId(R.id.textView)).check(matches(withText(name)))
    }

    @After
    fun tearDown() {
    }
}