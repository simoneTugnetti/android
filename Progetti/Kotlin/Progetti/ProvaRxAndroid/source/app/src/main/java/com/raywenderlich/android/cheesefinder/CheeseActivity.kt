/*
 * Copyright (c) 2019 Razeware LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * Notwithstanding the foregoing, you may not use, copy, modify, merge, publish,
 * distribute, sublicense, create a derivative work, and/or sell copies of the
 * Software in any work that is designed, intended, or marketed for pedagogical or
 * instructional purposes related to programming, coding, application development,
 * or information technology.  Permission for such use, copying, modification,
 * merger, publication, distribution, sublicensing, creation of derivative works,
 * or sale is expressly withheld.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

// https://medium.com/@gabrieldemattosleon/fundamentals-of-rxjava-with-kotlin-for-absolute-beginners-3d811350b701

package com.raywenderlich.android.cheesefinder

import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import io.reactivex.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.functions.BiFunction
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.activity_cheeses.*
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit

class CheeseActivity : BaseSearchActivity() {

    private lateinit var disposable: Disposable
    private lateinit var disposable2: Disposable
    private lateinit var disposable3: Disposable
    private lateinit var disposable4: Disposable
    private lateinit var disposable5: Disposable
    private lateinit var disposable6: Disposable
    private lateinit var disposable7: Disposable
    private lateinit var disposable8: Disposable
    private lateinit var disposable9: Disposable
    private lateinit var disposable10: Disposable
    private lateinit var disposable11: Disposable
    private lateinit var disposable12: Disposable
    private lateinit var disposable13: Disposable
    private lateinit var disposable14: Disposable
    private lateinit var disposable15: Disposable
    private lateinit var disposable16: Disposable
    private lateinit var disposable17: Disposable
    private lateinit var disposable18: Disposable
    private lateinit var disposable19: Disposable
    private lateinit var disposable20: Disposable
    private lateinit var disposable21: Disposable
    private lateinit var disposable22: CompositeDisposable

    private val tag = this::class.java.simpleName

    override fun onStart() {
        super.onStart()

        disposable = //createButtonClickObserver()
            // createTextChangeObservable()
            // Observable.merge(createButtonClickObservable(), createTextChangeObservable())
            Flowable.merge(
                createButtonClickObservable().toFlowable(BackpressureStrategy.LATEST),
                createTextChangeObservable().toFlowable(BackpressureStrategy.BUFFER)
            )
                // .subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext { showProgress() }
                .observeOn(Schedulers.io())
                .map { cheeseSearchEngine.search(it) }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    hideProgress()
                    showResult(it)
                }

        disposable2 = Observable.just("Apple", "Orange", "Banana")
            .doOnError { Log.e(tag, "Error: $it") }
            .doOnComplete { Log.i(tag, "Observable Just Completed") }
            .subscribe { Log.i(tag, "Received: $it") }

        disposable3 = Observable.fromArray("Apple", "Orange", "Banana")
            .doOnError { Log.e(tag, "Error: $it") }
            .doOnComplete { Log.i(tag, "Observable fromArray Completed") }
            .subscribe { Log.i(tag, "Received: $it") }

        disposable4 = Observable.fromIterable(listOf("Apple", "Orange", "Banana"))
            .doOnError { Log.e(tag, "Error: $it") }
            .doOnComplete { Log.i(tag, "Observable fromIterable Completed") }
            .subscribe { Log.i(tag, "Received: $it") }

        disposable5 = getObservableFromList(listOf("Apple", "", "Banana"))
            .doOnComplete { Log.i(tag, "Observable create Completed") }
            .subscribe (
                { Log.i(tag, "Received: $it") },
                { Log.e(tag, "Error: ${it.message}") }
            )

        disposable6 = Observable.intervalRange(10L, 5L, 0L, 1L,
            TimeUnit.SECONDS)
            .doOnComplete { Log.i(tag, "Observable intervalRange Completed") }
            .subscribe (
                { Log.i(tag, "Result just received: $it") },
                { Log.e(tag, "Error: ${it.message}") }
            )

        disposable7 = Observable.interval(1000, TimeUnit.MILLISECONDS)
            .doOnDispose { Log.i(tag, "Observable interval Stopped") }
            .subscribe (
                {
                    Log.i(tag, "Result just received: $it")
                    if (it == 10L) disposable7.dispose()
                },
                { Log.e(tag, "Error: ${it.message}") }
            )

        val publishSubject = PublishSubject.create<Int>()
        disposable8 = publishSubject
            .toFlowable(BackpressureStrategy.DROP)
            .observeOn(Schedulers.computation())
            .subscribe(
                { Log.i(tag, "The number is: $it") },
                { Log.e(tag, "Error: ${it.message}") },
                { Log.i(tag, "PublishSubject create computation completed") }
            )

        for (i in 0..10000) publishSubject.onNext(i)

        // Flowable support Backpressure
        disposable9 = Flowable.just("This is Flowable")
            .subscribe(
                { Log.i(tag, "Received: $it") },
                { Log.e(tag, "Error: ${it.message}") },
                { Log.i(tag, "Flowable just completed")  }
            )

        // Return single optional value, onSuccess (value), onComplete (no-value), onError (error)
        disposable10 = Maybe.just("This is Maybe")
            .subscribe(
                { Log.i(tag, "Received: $it") },
                { Log.e(tag, "Error: ${it.message}") },
                { Log.i(tag, "Maybe just completed")  }
            )

        // Single Value Emitted, onSuccess (value), onError (no-value)
        disposable11 = Single.just("This is Single")
            .subscribe(
                { Log.i(tag, "Received: $it, Single Completed") },
                { Log.e(tag, "Error: ${it.message}") }
            )

        // Only complete or error
        disposable12 = Completable.create {
            it.onComplete()
            // it.onError(Exception("Error Completable"))
        }.subscribe()

        disposable13 = Observable.just("Hello")
            .doOnSubscribe { println("Subscribed") }
            .doOnNext { s -> println("Received: $s") }
            .doAfterNext { println("After Receiving") }
            .doOnError { e -> println("Error: $e") }
            .doOnComplete { println("Complete Observer list method") }
            .doFinally { println("Do Finally!") }
            .doOnDispose { println("Do on Dispose!") }
            .subscribe { println("Subscribe") }

        disposable14 = Observable.just("Apple", "Orange", "Banana")
            .subscribeOn(Schedulers.io())
            //.subscribeOn(Schedulers.from(Executors.newFixedThreadPool(10)))
            //.subscribeOn(Schedulers.trampoline())
            //.subscribeOn(Schedulers.single())
            //.subscribeOn(Schedulers.computation())
            // .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { Log.i(tag, "Received: $it") }

        disposable14 = Observable.just("Orange", "Apple")
            .compose(applyObserverAsync())
            .subscribe { println("The First Observable Received: $it") }

        disposable15 = Observable.just("Water", "Fire")
            .compose(applyObserverAsync())
            .subscribe { println("The Second Observable Received: $it") }

        disposable16 = Observable.just("Water", "Air", "Fire")
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .map { "$it 2" }
            .subscribe { println("Received: $it") }

        disposable17 = Observable.just("Water", "Fire", "Wood")
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .flatMap { m ->
                Observable.just("$m 2")
                    .subscribeOn(Schedulers.io())
            }
            .subscribe { v -> println("Received: $v") }

        disposable18 = Observable.zip(
            Observable.just("Roses", "Sunflowers", "Leaves", "Clouds",
                "Violets", "Plastics"),
            Observable.just("Red", "Yellow", "Green", "White or Grey", 
                "Purple"),
            BiFunction<String, String, String> { type, color -> "$type are $color" }
        ).subscribe { println("Received: $it") }

        disposable19 = Observable.concat(
            Observable.just("Apple", "Orange", "Banana"),
            Observable.just("Microsoft", "Google"),
            Observable.just("Grass", "Tree", "Flower", "Sunflower")
        ).subscribe { println("Received: $it") }

        disposable20 = Observable.merge(
            Observable.interval(250, TimeUnit.MILLISECONDS).map { "Apple" },
            Observable.interval(150, TimeUnit.MILLISECONDS).map { "Orange" }
        ).take(10).subscribe{ v -> println("Received: $v") }

        disposable21 = Observable.just(2, 30, 22, 5, 60, 1)
            .filter{ x -> x < 10 }.subscribe{ x -> println("Received: $x") }

        Observable.just("Apple", "Orange", "Banana")
            .repeat(2).subscribe { v -> println("Received: $v") }.dispose()

        disposable22 = CompositeDisposable()
        disposable22.add(Observable.just("Apple", "Orange", "Banana")
            .subscribe { v -> println("Received: $v") })
        disposable22.add(Observable.just("Water", "Fire", "Air")
                .subscribe { v -> println("Received: $v") })
        disposable22.clear()

    }

    override fun onStop() {
        super.onStop()
        if (!disposable.isDisposed) disposable.dispose()
        if (!disposable2.isDisposed) disposable2.dispose()
        if (!disposable3.isDisposed) disposable3.dispose()
        if (!disposable4.isDisposed) disposable4.dispose()
        if (!disposable5.isDisposed) disposable5.dispose()
        if (!disposable6.isDisposed) disposable6.dispose()
        if (!disposable7.isDisposed) disposable7.dispose()
        if (!disposable8.isDisposed) disposable8.dispose()
        if (!disposable9.isDisposed) disposable9.dispose()
        if (!disposable10.isDisposed) disposable10.dispose()
        if (!disposable11.isDisposed) disposable11.dispose()
        if (!disposable12.isDisposed) disposable12.dispose()
        if (!disposable13.isDisposed) disposable13.dispose()
        if (!disposable14.isDisposed) disposable14.dispose()
        if (!disposable15.isDisposed) disposable15.dispose()
        if (!disposable16.isDisposed) disposable16.dispose()
        if (!disposable17.isDisposed) disposable17.dispose()
        if (!disposable18.isDisposed) disposable18.dispose()
        if (!disposable19.isDisposed) disposable19.dispose()
        if (!disposable20.isDisposed) disposable20.dispose()
        if (!disposable21.isDisposed) disposable21.dispose()
        if (!disposable22.isDisposed) disposable22.dispose()
    }

    // Transformers are also for other type, Flowable - Maybe...
    private fun <T> applyObserverAsync() =
        ObservableTransformer<T, T> {
            it.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
        }

    private fun createTextChangeObservable() =
        Observable.create<String> { emitter ->
            val textWatcher = object: TextWatcher {
                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) = Unit
                override fun afterTextChanged(p0: Editable?) = Unit

                override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                    p0?.toString()?.let { emitter.onNext(it) }
                }

            }

            queryEditText.addTextChangedListener(textWatcher)

            emitter.setCancellable {
                queryEditText.removeTextChangedListener(textWatcher)
            }

        }.filter { it.length >= 2 }.debounce(1000, TimeUnit.MILLISECONDS)

    private fun createButtonClickObservable() =
        Observable.create<String> { emitter ->
            searchButton.setOnClickListener {
                emitter.onNext(queryEditText.text.toString())
            }

            emitter.setCancellable {
                searchButton.setOnClickListener(null)
            }

        }

    private fun getObservableFromList(list: List<String>) =
        Observable.create<String> { emitter ->
            list.forEach {
                if (it.isBlank()) emitter.onError(Exception("No value to show"))
                emitter.onNext(it)
            }
            emitter.onComplete()
        }

}

/*
myObservable // observable will be subscribed on i/o thread
      .subscribeOn(Schedulers.io())
      .observeOn(AndroidSchedulers.mainThread())
      .map { /* this will be called on main thread... */ }
      .doOnNext{ /* ...and everything below until next observeOn */ }
      .observeOn(Schedulers.io())
      .subscribe { /* this will be called on i/o thread */ }
 */