import kotlinx.coroutines.*
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

/**
 * @author Simone Tugnetti
 */

/**
 * Coroutines
 * In kotlin è presente un metodo utilizzato per gestire i thread e l'avvio o il blocco di determinati
 * frammenti di codice chiamato {{ coroutines }}.
 * Normalmente, un'operazione all'interno del main viene eseguito in modo sincrono, cioè per poter eseguire
 * una specifica procedura, devono prima essere terminate quelle precedenti.
 * Usando invece le coroutines, il lavoro può essere suddiviso in thread per renderne l'esecuzione
 * più veloce e reattiva.
 */
fun main() {

    // Queste operazioni vengono avviate in modo sincrono, quindi sequenziale

    // exampleBlocking()

    // exampleBlockingDispatcher()

    // exampleLaunchGlobal()

    // exampleLaunchGlobalWaiting()

    // exampleLaunchCoroutinesScope()

    // exampleLaunchCoroutinesScopeCreate()

    exampleAsyncAwait()

    // exampleWithContext()
}

/**
 * Suspend
 * Viene utilizzato per definire una funzione che deve essere sospesa dall'esecuzione dal main thread
 */
suspend fun printDelayed(message: String) {
    // Delayed è una funzione delle coroutines utilizzata per dare un delay prima di poter continuare con
    // l'esecuzione del programma
    delay(1000)
    println(message)
}

suspend fun calculateHardThings(startNum: Int): Int {
    delay(1000)
    return startNum * 10
}

/**
 * runBlocking
 * Usando la dicitura runBlocking si intende un blocco di codice che viene avviato bloccando tutti i processi
 * nel main thread
 */
fun exampleBlocking() = runBlocking {
    println("one")
    printDelayed("two")
    println("three")
}

/**
 * runBlocking può anche viaggiare su thread separati da quello del main usando la dicitura {{ Dispatchers }},
 * questo infatti permette a un determinato codice di essere posizionato in un thread disponibile differente
 * da quello principale
 */
fun exampleBlockingDispatcher() {
    // Questo blocco di codice viaggia in un thread differente da quello del main
    runBlocking(Dispatchers.Default) {
        println("one - from thread ${Thread.currentThread().name}")
        printDelayed("two - from thread ${Thread.currentThread().name}")
    }

    // Fuori dal runBlocking per visualizzare che si sta eseguendo nel thread main bloccato
    println("three - from thread ${Thread.currentThread().name}")
    // Partirà solo quando sarà eseguito completamente runBlocking
}

/**
 * Launch
 * Viene utilizzato per eseguire un blocco di codice senza dover bloccare il main thread, questo implica però
 * che il thread principale possa terminare prima del termine del thread launch
 */
fun exampleLaunchGlobal() = runBlocking {
    println("one - from thread ${Thread.currentThread().name}")

    // Non blocca il main thread, lavora su un thread differente
    GlobalScope.launch {
        printDelayed("two - from thread ${Thread.currentThread().name}")
    }

    println("three - from thread ${Thread.currentThread().name}")

    delay(3000)
}

/**
 * Per poter terminare il programma quando ogni thread è effettivamente concluso, viene utilizzato il metodo join(),
 * unendo il thread di launch a quello del main per verificare quando quest'ultimo avrà terminato il proprio processo
 * per far così terminare il tutto
 */
fun exampleLaunchGlobalWaiting() = runBlocking {
    println("one - from thread ${Thread.currentThread().name}")

    val job = GlobalScope.launch {
        printDelayed("two - from thread ${Thread.currentThread().name}")
    }

    println("three - from thread ${Thread.currentThread().name}")

    // Come il delay, solo che in questo caso si attende che il processo sia terminato per poter terminare il programma
    job.join()
}

/**
 * In questo caso, launch lavora assieme al thread main, dato che non viene specificato da quale thread deve avviarsi.
 * In questo modo il thread principale sa di dover attendere il termine di ogni suo processo, non avendone altri a cui
 * collegarsi per attenderne il compimento
 */
fun exampleLaunchCoroutinesScope() = runBlocking {
    println("one - from thread ${Thread.currentThread().name}")

    launch {
        printDelayed("two - from thread ${Thread.currentThread().name}")
    }

    /* Usata per poter lavorare su più livelli di Thread
    launch(Dispatchers.Default) {
        printDelayed("two - from thread ${Thread.currentThread().name}")
    }
    */
    println("three - from thread ${Thread.currentThread().name}")

}

/**
 * È anche possibile poter creare dei nuovi thread da utilizzare nel caso in cui non ce ne siano di disponibili.
 * Alla fine delle loro utilità però, devono essere terminati, altrimenti il programma continuerà a lavorare
 * fino a uno shutdown
 */
fun exampleLaunchCoroutinesScopeCreate() = runBlocking {
    println("one - from thread ${Thread.currentThread().name}")

    val customDispatcher = Executors.newFixedThreadPool(2).asCoroutineDispatcher()

    launch(customDispatcher) {
        printDelayed("two - from thread ${Thread.currentThread().name}")
    }

    println("three - from thread ${Thread.currentThread().name}")

    (customDispatcher.executor as ExecutorService).shutdown()

}

/**
 * Async
 * Viene utilizzato, per l'appunto, a eseguire in modo asincrono determinate operazioni.
 * Il frammento di codice che si volesse avviare in modo asincrono può essere avviato simultaneamente
 * ad altre operazioni, rendendo il programma notevolmente ottimizzato.
 * Semplicemente, un blocco di codice deve essere inserito all'interno di async{} e fatto eseguire con await(),
 * questo rende l'esecuzione simultanea, accorciando i tempi
 */
fun exampleAsyncAwait() = runBlocking {
    val startTime = System.currentTimeMillis()

    val deferred1 = async { calculateHardThings(10) }
    val deferred2 = async { calculateHardThings(20) }
    val deferred3 = async { calculateHardThings(30) }

    val sum = deferred1.await() + deferred2.await() + deferred3.await()

    println("Somma totale: $sum")

    val endTime = System.currentTimeMillis()

    println("Time: ${endTime - startTime}")

}

/**
 * withContext
 * Viene utilizzato quando non vi è il bisogno di avviare simultaneamente più operazioni
 * ma anche che il programma continui a lavorare senza blocchi, dando come riferimento un diverso thread di lavoro.
 * La diversità da async è che withContext non restituisce un processo, bensì il diretto risultato di quello
 * che si trova all'interno del corpo dell'operazione
 */
fun exampleWithContext() = runBlocking {
    val startTime = System.currentTimeMillis()

    val deferred1 = withContext(Dispatchers.Default) { calculateHardThings(10) }
    val deferred2 = withContext(Dispatchers.Default) { calculateHardThings(20) }
    val deferred3 = withContext(Dispatchers.Default) { calculateHardThings(30) }

    val sum = deferred1 + deferred2 + deferred3

    println("Somma totale: $sum")

    val endTime = System.currentTimeMillis()

    println("Time: ${endTime - startTime}")

}
