package instructionBase

import kotlin.random.Random

/**
 * @author Simone Tugnetti
 */

fun main() {

    /**
     * Collection
     * Insieme di informazioni, più comunemente Liste
     */
    val collection = listOf(1,2,3,4)
    val listMod = mutableListOf(5,6,7,8)

    /**
     * Set
     * Lista di elementi non ordinati per ricavarne solo l'intera lista
     * invece dei singoli elementi
     */
    val set = setOf(1,2,3)
    val setMod = mutableSetOf(4,5,6)

    /**
     * Sequence
     * Oggetto che contiene un insieme di informazioni ma che non necessita
     * di una ricreazione dell'intero insieme nel caso di una verifica o ricerca
     * degli elementi specifici al suo interno
     */
    val sequence = collection.asSequence()

    val newSeq = sequence
        .map { it * it }

    println(newSeq)
    println(newSeq.maxOrNull())

    /**
     * Una sequenza non ricrea la lista in map
     * ma crea il primo valore, lo analizza in find
     * e se non è soddisfatto passa al secondo valore della sequenza
     */
    val newSeq2 = sequence
        .map { it * it }
        .find { it > 3 }

    val seq = generateSequence {
        Random.nextInt(5).takeIf { it > 0 }
    }

    val seq2 = generateSequence(3) { n ->
        (n + 1).takeIf { it < 7}
    }

    val seq3 = sequence {
        yield(1000)

        // Calls only if necessary in map or filter
        yieldAll(0..5)
        yieldAll(listOf(3,5,6))
    }

    println(seq.toList())
    println(seq2.toList())
    println(seq3.toList())

}