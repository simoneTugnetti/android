package instructionBase

import java.awt.Point

/**
 * @author Simone Tugnetti
 */

/**
 * Operation Function
 * Funzione di estensione che permette di eseguire operazioni con gli oggetti
 * presi in considerazione.
 * Tipi di chiamate:
 * plus -> a + b
 * minus -> a - b
 * times -> a * b
 * div -> a / b
 * mod -> a % b
 * plusAssign -> a += b -> a = a + b
 */
operator fun Point.plus(other: Point): Point {
    return Point(x + other.x, y + other.y)
}

/**
 * Unary Operator
 * In più ai precedenti, vi sono anche gli operatori unari, cioè senza passaggio di parametri
 * Tipi di chiamate:
 * unaryPlus -> +a
 * unaryMinus -> -a
 * not -> !a
 * inc -> ++a, a++
 * dec -> --a, a--
 */
operator fun Point.unaryMinus() = Point(-x, -y)

data class Contact(val name: String, val email: String, val phoneNumber: String)

fun main() {
    // a.set(Point)
    val a = Point(1,2) + Point(1,2)
    println("${a.x} ${a.y}")

    -Point(3,4)

    val p = (0 to 0)

    val (b, c) = p
    val d = p.component1()
    val e = p.component2()

    b == c
    b.equals(c)

    b..c
    b.rangeTo(c)

    val (name, _, phoneNumber)= Contact("gianni", "gg@esa.com", "000")

}