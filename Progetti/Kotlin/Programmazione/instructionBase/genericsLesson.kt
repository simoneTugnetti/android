package instructionBase

/**
 * @author Simone Tugnetti
 */

fun main() {
    val person: GenericPerson<Int> = GenericPerson(30)
    val personString: GenericPerson<String> = GenericPerson("30")

    sort(30)
    sort(35.2)
    // `instruction-base`.sort("ciao") -> Darà errore, dato che sono state assegnate delle limitazioni
}

/**
 * Generics
 * Il tipo generico in kotlin si definisce con {{ T }} e viene utilizzato quando una variabile può assumere più tipi
 * differenti, come string o Int, senza il bisogno di doverla ricreare molte volte
 */
data class GenericPerson<T>(var data: T)

/**
 * Funzioni Generics
 * Queste ragionano allo stesso modo delle classi, ma deve venir specificato che la funzione è di tipo generics
 * prima del nome.
 * Possono essere aggiunte anche delle limitazioni di tipo usando la dicitura {{ where }} per definire i tipi ammessi
 * per la variabile che si andrà a utilizzare
 */
fun <T> sort(data: T) where T: Number, T: Comparable<T> { }

/**
 * Funzione generica nullable
 */
fun <E> user(elem: E): E {
    return elem
}

/**
 * Funzione generica non nullable di tipo Any
 * T == Any
 */
fun <T: Any> user2(elem: T): T {
    return elem
}

/**
 * Funzione generica nullable di tipo Number
 * T == Number?
 */
fun <T: Number?> number(elem: T): Double {
    return elem as Double
}