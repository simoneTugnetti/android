package instructionBase

/**
 * @author Simone Tugnetti
 */
fun main() {
    val firstPerson = DataPerson("Simone", 23, "Sviluppatore")
    val secondPerson = DataPerson("Giovanni", 22, "Cameriere")

    val olderPerson = if (firstPerson.age >= secondPerson.age) firstPerson else secondPerson
    olderPerson.printPerson()


    // Esegue il blocco e utilizza subito ciò che ritorna
    run {
        if (firstPerson.age >= secondPerson.age) firstPerson else secondPerson
    }.printPerson()


    // Utilizzando un elemento come parametro, è possibile eseguire operazioni usando l'oggetto
    // in questione oppure eseguire altre operazioni usando gli elementi di tale oggetto.
    with(firstPerson) {
        age += 1
        "Age is now $age"
    }.println()

    // Stessa modalità di with, maggiormente utilizzato
    firstPerson.run {
        age += 1
        "Age is now $age"
    }.println()

    // Come il run usando una variabile tramite lambda
    firstPerson.let { person ->
        person.age += 1
        "Age is now ${person.age}"
    }.println()

    // Vi è la possibilità di verificare anche se una variabile è di un determinato tipo o nulla
    val num: Any? = 4
    (num as? Int)?.let {
        println(it + it)
    }

    // Uguale a num != null
    num?.let {
        "NUM: $it"
    }

    // Usato per modificare gli elementi all'interno dell'oggetto oppure l'elemento stesso senza
    // doverlo richiamare più volte. Ritorna l'oggetto stesso modificato
    secondPerson.apply {
        age += 1
        job = "Barman"
    }.printPerson()

    // Stesso risultato della precedente, ma con ripetizioni
    // secondPerson.age += 1
    // secondPerson.job = "Non so"

    // Come apply ma usando una variabile di lambda
    secondPerson.also { anotherPerson ->
        anotherPerson.age += 1
        anotherPerson.job = "Disoccupato"
    }.printPerson()
}

data class DataPerson(
    var name: String,
    var age: Int,
    var job: String
) {
    fun printPerson() = println(toString())
}

fun String.println() = println(this)
