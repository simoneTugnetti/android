package instructionBase

/**
 * @author Simone Tugnetti
 */

fun lambdas() {
    val list = mutableListOf(1,2,3,3)
    val list2 = listOf(5,6,7,8)

    val distinct = list.distinct()

    val newList = list.map { it * it }

    val value = list.find { it % 2 == 0 }

    val filter = list.filter { it % 2 == 0 }

    val checkElem = list.any{ it % 2 == 0 }

    val firstElem = list.first { it % 2 == 0 }

    val count = list.count { it % 2 == 0 }

    val partition = list.partition { it % 2 == 0 }

    val group = list.groupBy { 2 }

    val associateBy = list.associateBy { 3 }

    val associate = list.associate { 'a' + it to 10 * it }

    val zip = list.zip(list2)

    val zipWithNext = list.zipWithNext()

    val max = list.maxByOrNull { it }

    val all = list.all { it > 1 }

    val map = mapOf("Gianni" to 12, "GG" to 12)

    val get = map.getOrElse("DD") { 99 }

    val allPossiblePairs = map.flatMap { first ->
        map.map { second -> first to second }
    }

}

fun funLambda() {
    val sum: (Int, Int) -> Int = { x,y -> x + y }

    val newSum = sum(2,3)

    val isEven: (Int) -> Boolean = { i -> i % 2 == 0}

    val example = isEven(42)

    val f1: () -> Int? = { null }
    val f2: (() -> Int)? = null

    fun isEvenFun(i: Int): Boolean = i % 2 == 0

    val predicate = ::isEvenFun

    val pred2 = { i: Int -> isEvenFun(i) }

    val newPredicate = predicate(12)

    val agePredicate = Person::isOlder

    val agePredicate2: (Person, Int) -> Boolean = {
            person, ageLimit -> person.isOlder(ageLimit)
    }

    agePredicate(Person("Alice", 29), 21)

    val alice = Person("Alice", 29)
    val agePredicate3 = alice::isOlder
    agePredicate3(21)

    val agePredicate4 = alice.getAgePredicate()
    agePredicate4(12)

}

class Person(val name: String, val age: Int) {
    fun isOlder(ageLimit: Int) = age > ageLimit

    fun getAgePredicate() = ::isOlder

}

fun duplicateNonZero(list: List<Int>): List<Int> {
    return list.flatMap {
        if (it == 0) return@flatMap listOf()
        listOf(it, it)
    }
}

fun duplicateNonZeroAlt(list: List<Int>): List<Int> {
    return list.flatMap(fun (e): List<Int> {
        if (e == 0) return listOf()
        return listOf(e, e)
    })
}

fun main() {
    listOf<Int>().any {
        it > 0
    }

    val map = mutableMapOf<String, String>()
    map["Gianni"] = "Mucci"

    map.mapValues {
        "${it.key}, ${it.value}"
    }

    map.mapValues { (_, value) ->
        "Valore: $value"
    }

    lambdas()

    duplicateNonZero(listOf(3, 0, 5))
    duplicateNonZeroAlt(listOf(4, 6, 0))

}