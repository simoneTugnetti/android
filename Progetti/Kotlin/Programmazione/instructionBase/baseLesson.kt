package instructionBase

import java.lang.IllegalArgumentException

/**
 * @author Simone Tugnetti
 */

fun main() {

    /**
     *  Tipi di variabile:
     *  Val --> significa variabile read-only e può essere inizializzata solo una volta
     *  Var --> variabile che può essere modificata dopo l'inizializzazione
     */
    val valore: String = 1.toString()
    println(valore)
    //valore = "si" --> Non funzionerà, perché ((val)) è read-only

    /**
     *  Tipi di dato:
     *  Come java (String, Float, Int) ma al posto di Object --> Any
     */
    var numero: Any = 12.5f  //float
    numero = 12.5  //double
    numero = "no"

    if (numero is Number) {
        numero.toDouble()
    }

    /**
     * Tipi Unit e Nothing
     * Unit è equivalente a Void, esegue una sequenza di operazioni
     * Nothing è un tipo senza alcun ritorno, può solo richiamare eccezzioni
     * A differenza di Unit, non esegue un cast di tipo Any ad una variabile con if branch, ma direttamente
     * il tipo del valore true, dato che Nothing non ha "tipo"
     */
    fun exception(message: String): Nothing {
        throw IllegalArgumentException(message)
    }

    val unit: Unit = println("Hey")

    val num = if (3 in 0..5) {
        45
    } else {
        exception("Not in range")
    }

    /**
     *
     * Tipo di dato:
     * Byte  -->  peso all'interno della RAM -> 8 bit
     * Short  -->  peso -> 16 bit
     * Int  -->  peso -> 32 bit
     * Long  -->  peso -> 64 bit
     *
     */
    val byt: Byte = 1
    val long: Long = 2000000
    val int: Int = 1
    val short: Short = 10

    /**
     * Pair or Triple
     * Oggetto od assegnazione contenente due o tre variabili
     * al suo interno
     */
    val (a, b) = "Giovanni" to 9
    val (c, d, e) = Triple(15.5f, 20.7, false)

    /**
     * Array:
     * Gli array possono essere creati tramite arrayOf( valori ) o Array( size, valori )
     */
    val array1: Array<Int> = arrayOf(1,2,3,4,5,6,7,8,9,0);
    val array2: Array<Int> = Array(10) { i -> i*2}
    val singleVal1: Int = array1.get(2)
    val singleVal2: Int = array2[1]
    println("Stampa memoria array: $array1")

    /**
     * Funzioni
     * Le funzioni possono essere create tramite fun() e il tipo di valore ritornato tramite fun(): Any
     * Le condizioni usate in kotlin possono essere salvate anche all'interno di variabili,
     * togliendo il ritorno all'interno delle condizioni e scrivendolo solo alla fine della funzione
     */
    fun max(a: Int, b: Int): Int {
        val answer = if (a > b) {
            a
        } else if(b > a) {
            b
        } else{
            a
        }

        return answer
    }

    /**
     * Carattere $
     * Questo carattere può essere utilizzato per includere una variabile, o una funzione, all'interno di una stringa
     */
    val max: Int = max(10,23)
    println("\nIl valore maggiore in variabile è $max")
    println("Il valore maggiore in funzione è ${max(10,45)}")

    /**
     * Funzione usata per capire il funzionamento di when
     */
    fun getVal(value: Int): Int {
        println("Dato ritornato usando una funzione")
        return value
    }

    /**
     * When
     * Espressione utilizzata per gestire i dati come se fossero all'interno di uno switch,
     * Questa può anche essere salvata in una variabile di ritorno come nell'esempio dell'if
     * oppure ritornata direttamente.
     * Il valore può anche essere preso come ritorno da una funzione, esattamente come in java
     *
     */
    fun printNum(a: Int): String {
         return when (a) {
            getVal(a) -> "E' uguale a 1"
            2 -> "E' uguale a 2"
            else -> {
                "E' un altro numero"
            }
        }
    }

    println(printNum(5))
    println("Valore con funzione -> ${printNum(1)}")

    /**
     * Range
     * Dato utilizzato per salvare o visualizzare dati come fossero un valore compreso tra 1 e 5
     * Ad esempio, 1..5
     */
    fun rangeOfValor(): IntRange {
        return 0..100
    }
    println(rangeOfValor())

    /**
     * Progression
     * Dato utilizzato per visualizzare il range di dati anche all'inverso utilizzando l'espressione downTo
     * Ad esempio, 5 downTo 1
     */
    fun rangeOfValorDownTo(): IntProgression {
        return 100 downTo 0
    }
    println(rangeOfValorDownTo())

    /**
     * Step
     * Usato nei range di valori, salva i valori all'interno della lista saltando il numero di valori dopo lo step
     * Ad esempio, 1..100 step 5
     */
    fun rangeOfValorStep(): IntProgression {
        return 0..100 step 5
    }
    println(rangeOfValorStep())

    /**
     * Until
     * Usato nei range di valori, salva un valore da dall'inizio alla fine, senza conteggiare l'ultimo valore dato
     * Ad esempio, 0 until 100
     */
    fun rangeOfValorUntil(): IntProgression {
        return 0 until 100
    }
    println(rangeOfValorUntil())

    fun rangeOfValorMix(): IntProgression {
        return 100 downTo 0 step 5
    }
    println(rangeOfValorMix())

    /**
     * Cicli
     * La logica di un ciclo è uguale a quella vista in java, ma con la sintassi simile a swift
     * Un esempio, for(int i: List<int>()))  -->  for(i in List<Int>())
     */
    for (item in rangeOfValor()) {
        print("$item ")
    }

    println()

    for (item in rangeOfValorDownTo()) {
        print("$item ")
    }

    println()

    for (item in rangeOfValorStep()) {
        print("$item ")
    }

    println()

    for (item in rangeOfValorUntil()) {
        print("$item ")
    }

    println()

    for (item in rangeOfValorMix()) {
        print("$item ")
    }

    println("\nProva di stampa array in For indice")

    for (i in array2.indices) {
        print("$i ")
    }

    println("\nProva stampa ciclo For indice-valore")

    for (i in array2.indices) {
        println("$i -> ${array2[i]}")
    }



    println("\nProva stampa ciclo For indice-valore con withIndex()")

    for ((index, value) in array2.withIndex()) {
        println("Valore alla posizione $index: $value")
    }

    /**
     * While
     * Esattamente come in java, precondizione true con while e post-condizione true con do - while
     */
    fun whileFunction() {
        var cond = 0
        println("\nWhile Loop")
        while (cond <= 10) {
            print("$cond ")
            cond++
        }
        println("\nDo - While Loop")
        do {
            print("$cond ")
            cond--
        } while (cond >= 0)
    }

    whileFunction()

    /**
     * Repeat
     * Funzione che ripete una determinata azione tante volte quante quelle richieste
     */
    fun repeatFunction() {
        var cond = 0
        println("\nRepeatFunction")
        repeat(10) {
            print("$cond ")
            cond++
        }
    }

    repeatFunction()

}