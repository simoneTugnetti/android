/**
 * @author Simone Tugnetti
 */

fun game(str1: String, str2: String): Map<Int, Char> {
    val map = mutableMapOf<Int, Char>()
    val (newStr1, newStr2) =
        str1.toList() to str2.toList()
    if (newStr1.size >= newStr2.size) {
        for ((i, value) in newStr2.withIndex()) {
            if (value == newStr1[i]) {
                map[i] = value
            }
        }
    } else {
        for ((i, value) in newStr1.withIndex()) {
            if (value == newStr2[i]) {
                map[i] = value
            }
        }
    }
    return map
}

fun main() {
    val map = game("Cuao", "Miao")

    for ((i, value) in map) {
        println("Same char <$value> in position $i")
    }

    println("They share ${map.size} letters in the same position")

}