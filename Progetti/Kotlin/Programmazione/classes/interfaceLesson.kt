package classes

/**
 * @author Simone Tugnetti
 */

fun main() {
    val lampadina: Interruttore = Lampadina()
    val televisore: Interruttore = Televisore()
    val switch = Switch()
    switch.on(lampadina)
    switch.off(televisore)
}

/**
 * Interfacce
 * Un'interfaccia può essere utilizzata come in java, cioè contenente metodi astratti da implementare successivamente
 * nelle classi da cui è ereditata.
 * I metodi devono quindi essere sovrascritti per renderli utilizzabili
 */
interface Interruttore {
    fun on()
    fun off()
}

class Lampadina: Interruttore {
    override fun on() {
        println("classes.Lampadina Accesa")
    }

    override fun off() {
        println("classes.Lampadina Spenta")
    }

}

class Televisore: Interruttore {
    override fun on() {
        println("classes.Televisore Acceso")
    }

    override fun off() {
        println("classes.Televisore Spento")
    }

}

class Switch {
    fun on(interruttore: Interruttore) {
        interruttore.on()
    }

    fun off(interruttore: Interruttore) {
        interruttore.off()
    }
}
