package classes

/**
 * @author Simone Tugnetti
 */

fun main() {
    val student = Studente("Milo", 21, 1010)
    println(student.printPerson())
}

/**
 * Ereditarietà
 * Questa è esattamente come in java, prende o assegna i valori dalla super-classe ma con qualche differenza.
 * La principale è che in kotlin tutte le classi sono statiche o final, perciò per renderle aperte alla possibilità
 * di essere utilizzate come ereditarie, bisogna inserire la dicitura {{ open }} prima di richiamare la suddetta classe
 */
open class Person (name: String, age: Int) {
    var name: String = ""
    var age: Int = 0
    var id: Int = 0

    init {
        this.name = name
        this.age = age
    }

    constructor(name: String, age: Int, id: Int): this(name, age) {
        this.id = id
    }

    open fun printPerson(): String {
        return "Nome: $name, Età: $age, ID: $id"
    }

}

/**
 * Sotto-classe
 * Le sottoclassi prendono tutti i dati dalla super-classe specificando sia con this, dato che la sottoclasse
 * prenderebbe le informazioni dalla super-classe, o con super, richiamando direttamente le info dalla super-classe.
 * Questa eredita anche le possibili funzioni che si andrebbero a creare.
 * Ulteriormente, è possibile riscrivere determinate funzioni usando {{ override }}, queste possono quindi prendere
 * le informazioni dalla loro super-classe per poi restituire info più dettagliate, o semplicemente riscriverle
 */
class Studente (name: String, age: Int, id: Int): Person(name, age) {
    init {
        this.id = id
    }

    override fun printPerson(): String {
        return "Tipo: classes.Studente, ${super.printPerson()}"
    }

}