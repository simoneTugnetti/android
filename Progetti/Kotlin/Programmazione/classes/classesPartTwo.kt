package classes

/**
 * @author Simone Tugnetti
 */

/**
 * Sealed Class
 * Tutte le sottoclassi della superclasse Sealed devono essere all'interno dello stesso file
 */
sealed class Expr
class Num(val value: Int): Expr()
class Sum(val left: Expr, val right: Expr): Expr()

fun eval(e: Expr): Int = when (e) {
    is Num -> e.value
    is Sum -> eval(e.left) + eval(e.right)
}

interface Factory<T> {
    fun create(): T
}

/**
 * Companion Object
 * Singleton in grado di richiamare le informazioni al suo interno staticamente,
 * cioè senza necessità di istanza della classe genitore
 */
class A {

    // Richiama la funzione tramite istanza
    fun foo() = 0

    companion object: Factory<A> {
        fun foo() = 1

        // Access Static in java
        @JvmStatic fun foo2() = 2

        override fun create(): A {
            return A()
        }

    }

}


interface Repository {
    fun foo()
}

class RepositoryImpl: Repository {
    override fun foo() {
        println("Hi")
    }

}

/**
 * Implementation by delegation
 * Implementa l'interfaccia tramite delegazione usando la keyword (( by ))
 * ed una istanza dell'interfaccia da delegare
 */
class Controller(repository: Repository): Repository by repository

/**
 * Singleton
 * Classe inizializzata ed implementata una volta sola, per poi essere richiamata la stessa istanza
 * ogni volta ve ne è il bisogno
 */
object Singleton {
    val name: String = "Ciao"
}

fun main() {

    // Call static method
    A.foo()

    // Calls instance method
    A().foo()

    val rep = RepositoryImpl()
    val controller = Controller(rep).foo()

}