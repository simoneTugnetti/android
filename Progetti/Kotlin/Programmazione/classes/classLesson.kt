package classes

/**
 * @author Simone Tugnetti
 */

fun main() {
    val student1 = Student("Giorgio", "Casacchi")
    val student2 = SecondStudent("Guido", "Borchi")
    val student3 = SecondStudent("Gianni", "Pinotto", 25)
    val student4 = ThirdStudent("Bibbo", "Ascani", 30)
    println("\nIl nome dello studente ${student1.firstName}, il cognome ${student1.lastName}")
    println("Il nome del secondo studente ${student2.firstName}, il cognome ${student2.lastName}")
    println("Il nome dello studente con il secondo costruttore ${student3.firstName}, il cognome ${student3.lastName}, l'età ${student3.age}")
    student4.printStudent()
    val persona = Persona("Gianni", 40)
    println("\nQuesta persona si chiama ${persona.name} e ha ${persona.age} anni")
    println()
    val anotherPerson = AnotherPerson("Giovanni", 42)
    anotherPerson.InnerName().printPerson()
}


/**
 * Classi
 * Queste simili alla programmazione java, ma con qualche differenza
 * Prima tra tutte il costruttore, quest'ultimo, chiamato primary constructor, viene specificato nel nome della classe, rendendola di fatto unica
 * per il numero di parametri iniziali.
 * Possono essere però creati blocchi d'inizializzazione all'interno della stessa classe, inserendo la dicitura init,
 * per poter eseguire delle operazioni iniziali.
 * Le classi in kotlin vengono eseguite in modo procedurale, quindi si eseguiranno le operazioni in base alla loro
 * coda di scrittura.
 */
class Student constructor(firstName: String, lastName: String) {

    init {
        println("Primo initializer classes.Student")
        // Non possono essere associate variabili scritte dopo questo initializer
    }

    var firstName: String = ""
    var lastName: String = ""
    var age: Int = 25
    var address: String = ""
    var id: Long = 1234567890

    init {
        println("Secondo initializer classes.Student")
        this.firstName = firstName
        this.lastName = lastName
    }

}

/**
 * Assegnazione Parametri e Ulteriori Costruttori
 * Nel costruttore di una classe possono anche essere inizializzate delle variabili da utilizzare
 * successivamente senza il bisogno di specificarle all'interno della suddetta.
 * Inoltre, è possibile inserire più di un costruttore all'interno di una classe, se però quest'ultima ne possiede già
 * uno, bisogna specificare il ritorno, dando le variabili inizializzate precedentemente inserendo
 * la dicitura this(nome_variabile)
 */
class SecondStudent (var firstName: String = "", var lastName: String = "") {

    var age: Int = 0
    var address: String = ""
    var id: Long = 1234567890

    constructor(firstName: String, lastName: String, age: Int): this(firstName, lastName) {
        println("Secondo Costruttore classes.SecondStudent")
        this.age = age
    }

}

@Suppress("ConvertSecondaryConstructorToPrimary")
class ThirdStudent {

    var age: Int = 0
    var address: String = ""
    var id: Long = 1234567890
    var firstName: String = ""
    var lastName: String = ""
        set(value) {
            field = value
        }
        get() = field

    constructor(firstName: String, lastName: String, age: Int) {
        println("Costruttore classes.ThirdStudent")
        this.age = age
        this.firstName = firstName
        this.lastName = lastName
    }

    fun printStudent() {
        println("Il nome del terzo studente $firstName, il cognome $lastName, l'età $age")
    }

}

/**
 * Data class
 * Classe utilizzata come guida per identificare quali elementi deve avere una variabile.
 * Una Data Class viene creata solo tramite
 * Ad esempio, una persona ha un nome e un'età, questi dati possono essere salvati utilizzandoli successivamente
 */
data class Persona (var name: String, var age: Int)
data class StudenteData (var name: String, var id: Int)

/**
 * Inner Class
 * Modalità con cui si crea una classe interna a una già creata, nella quale è possibile poter utilizzare i dati
 * contenuti all'interno della sua classe genitore, un po' come funziona con l'ereditarietà
 */
class AnotherPerson (name: String, var age: Int = 0) {
    var name: String = ""

    init {
        this.name = name
    }

    inner class InnerName {
        var anotherName = name

        fun printPerson() {
            println("Inner -> Questa persona si chiama $anotherName e ha $age anni")
        }

    }

}

/**
 * Lateinit
 * Permette di inizializzare successivamente una variabile (( var ))
 * Utilizzato perlopiù in Android
 */
class ClassLateinit {
    lateinit var variable: String

    fun initialize() {
        println(::variable.isInitialized)
        variable = "init"
        println(::variable.isInitialized)
    }
}

/**
 * Visibilità
 * In kotlin ne esistono quattro tipi
 * private -> Visibile solo all'interno della classe, ad esempio in una class{} oppure in un singolo file
 * public -> Default, visibile ovunque all'interno del progetto
 * protected -> Come private, ma i dati possono essere visti anche dalle sottoclassi (ereditarietà)
 * internal -> I dati possono essere visti solo all'interno dello stesso package, ad esempio solo in com.example.io
 */
open class Visibility {
    private var name: String = ""
    public var surname: String = ""
    protected var age: Int = 0
    internal var id: Int = 0
}

class SubVisibility: Visibility() {
    init {
        age = 40
    }
}


