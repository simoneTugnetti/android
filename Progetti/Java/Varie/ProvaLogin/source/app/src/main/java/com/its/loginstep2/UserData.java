package com.its.loginstep2;

/**
 * @author Simone Tugnetti
 */
class UserData {

    private static final UserData ourInstance = new UserData();

    static UserData getInstance() {
        return ourInstance;
    }

    private User data;

    private UserData() {
        data=new User();
    }

    public User info(){
        return data;
    }

}
