package com.its.loginstep2;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.github.ybq.android.spinkit.SpinKitView;

/**
 * @author Simone Tugnetti
 */
public class RegistratiActivity extends AppCompatActivity {

    private EditText nome,cognome,telefono,mail,password,confPassword,indirizzo;
    private Button regEAccedi;
    private View oscurita;
    private SpinKitView caricamento;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrati);
        nome = findViewById(R.id.nome);
        cognome = findViewById(R.id.cognome);
        telefono = findViewById(R.id.telefono);
        mail = findViewById(R.id.mail);
        password = findViewById(R.id.password);
        confPassword = findViewById(R.id.confPassword);
        regEAccedi = findViewById(R.id.regEAccedi);
        oscurita = findViewById(R.id.oscurita);
        caricamento = findViewById(R.id.caricamento);
        indirizzo = findViewById(R.id.indirizzo);

        nome.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                resetHint(nome,"Inserisci il nome");
            }
        });

        cognome.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                resetHint(cognome,"Inserisci il cognome");
            }
        });

        mail.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                resetHint(mail,"Inserisci l'indirizzo e-mail");
            }
        });

        telefono.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                resetHint(telefono,"Inserisci il numero di telefono");
            }
        });

        password.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                resetHint(password,"Inserisci la password");
            }
        });

        confPassword.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                resetHint(confPassword,"Conferma la password");
            }
        });

        indirizzo.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                resetHint(indirizzo,"Inserisci l'indirizzo d'abitazione");
            }
        });

        regEAccedi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (elemeMancanti(nome,cognome,telefono,mail,password,confPassword,indirizzo)) {
                    erroreElem(nome,cognome,telefono,mail,password,confPassword,indirizzo);
                } else if (mailValida(mail)) {
                    alert("Attenzione","Formato e-mail non vaildo!");
                    errore(mail,"Reinserisci il tuo indirizzo e-mail");
                } else if (telValido(telefono)) {
                    alert("Attenzione","Numero di telefono non valido!");
                    errore(telefono,"Reinserisci il tuo numero di telefono");
                }else if(passSupSei(password) ){
                    alert("Attenzione","La password deve contenere almeno sei caratteri!");
                    errore(password,"Formato password non valido");
                    errore(confPassword,"Reinserisci la password");
                }else if(coincidaPass(password,confPassword)){
                    alert("Attenzione","Le password non coincidono!");
                    errore(confPassword,"Reinserisci la password");
                }else{
                    successo();
                }
            }
        });
    }

    private void resetHint(EditText testo, String msg){
        testo.setHintTextColor(ContextCompat.getColor(this,R.color.colorHint));
        testo.setHint(msg);
    }

    private void successo(){
        disattiva();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                attiva();
                alertInvio("Successo","Registrazione avvenuta con successo");
            }
        },3000);
    }

    private void alertInvio(String titolo, String msg){
        new AlertDialog.Builder(this).setTitle(titolo).setMessage(msg)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                invio();
            }
        }).show();
    }

    private void invio(){
        Intent intent = new Intent(this,MainActivity.class);
        salva();
        startActivity(intent);
    }

    private void salva(){
        UserData data = UserData.getInstance();
        data.info().setNome(nome.getText().toString());
        data.info().setCognome(cognome.getText().toString());
        data.info().setMail(mail.getText().toString());
        data.info().setPassword(password.getText().toString());
        data.info().setTelefono(telefono.getText().toString());
        data.info().setIndirizzo(indirizzo.getText().toString());
    }


    private void attiva(){
        caricamento.setVisibility(View.INVISIBLE);
        oscurita.setVisibility(View.INVISIBLE);
        regEAccedi.setVisibility(View.VISIBLE);
        nome.setEnabled(true);
        cognome.setEnabled(true);
        telefono.setEnabled(true);
        mail.setEnabled(true);
        password.setEnabled(true);
        confPassword.setEnabled(true);
        indirizzo.setEnabled(true);
    }

    private void disattiva(){
        caricamento.setVisibility(View.VISIBLE);
        oscurita.setVisibility(View.VISIBLE);
        regEAccedi.setVisibility(View.INVISIBLE);
        nome.setEnabled(false);
        cognome.setEnabled(false);
        telefono.setEnabled(false);
        mail.setEnabled(false);
        password.setEnabled(false);
        confPassword.setEnabled(false);
        indirizzo.setEnabled(false);
    }

    private void alert(String titolo, String msg){
        new AlertDialog.Builder(this).setTitle(titolo).setMessage(msg)
                .setPositiveButton("OK", null).show();
    }

    private void erroreElem(EditText nome, EditText cognome, EditText telefono, EditText mail,
                            EditText password, EditText confPassword,EditText indirizzo){
        if(nome.getText().toString().equals("")){
            errore(nome,"Nome mancante");
        }
        if(cognome.getText().toString().equals("")){
            errore(cognome,"Cognome mancante");
        }
        if(telefono.getText().toString().equals("")){
            errore(telefono,"Numero di telefono mancante");
        }
        if(mail.getText().toString().equals("")){
            errore(mail,"Indirizzo e-mail mancante");
        }
        if(password.getText().toString().equals("")){
            errore(password,"Password mancante");
        }
        if(confPassword.getText().toString().equals("")){
            errore(confPassword,"Conferma password mancante");
        }
        if(indirizzo.getText().toString().equals("")){
            errore(indirizzo,"Indirizzo d'abitazione mancante");
        }
    }

    private void errore(EditText testo, String msg){
        testo.setText("");
        testo.setHintTextColor(ContextCompat.getColor(this,R.color.errore));
        testo.setHint(msg);
    }

    private boolean mailValida(EditText mail){
        return !Patterns.EMAIL_ADDRESS.matcher(mail.getText().toString()).matches();
    }

    private boolean telValido(EditText telefono){
        return !Patterns.PHONE.matcher(telefono.getText().toString()).matches();
    }

    private boolean coincidaPass(EditText password, EditText confPassword){
        return !password.getText().toString().equals(confPassword.getText().toString());
    }

    private boolean passSupSei(EditText password){
        return password.getText().length()<6;
    }

    private boolean elemeMancanti(EditText nome, EditText cognome, EditText telefono,
                                  EditText mail, EditText password, EditText confPassword,
                                  EditText indirizzo){
        return nome.getText().toString().equals("") || cognome.getText().toString().equals("") ||
                telefono.getText().toString().equals("") || mail.getText().toString().equals("") ||
                password.getText().toString().equals("") || confPassword.getText().toString().equals("")
                || indirizzo.getText().toString().equals("");
    }
}
