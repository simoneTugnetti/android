package com.its.loginstep2;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.TextView;

/**
 * @author Simone Tugnetti
 */
public class UserActivity extends AppCompatActivity {

    private WebView mappa;
    private UserData data;
    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);
        TextView email = findViewById(R.id.email);
        TextView telefono = findViewById(R.id.telefono);
        TextView indirizzo = findViewById(R.id.indirizzo);
        mappa=findViewById(R.id.mappa);
        Button esci = findViewById(R.id.esci);
        Button provModifica = findViewById(R.id.provModifica);
        settaTesto(email, telefono, indirizzo);
        settaMappa();

        provModifica.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent=new Intent(UserActivity.this,ModificaActivity.class);
                startActivity(intent);
            }
        });

        email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(Intent.ACTION_SEND);
                intent.setType("message/rfc822");
                startActivity(intent);
            }
        });

        telefono.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                data=UserData.getInstance();
                intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel: "+data.info().getTelefono()));
                startActivity(intent);
            }
        });

        indirizzo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse("http://maps.google.com/maps?saddr=20.344,34.34&daddr=20.5666,45.345"));
                startActivity(intent);
            }
        });

        esci.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(UserActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void settaMappa(){
        mappa.loadUrl("file:///android_asset/index.html");
        WebSettings webSettings = mappa.getSettings();
        webSettings.setJavaScriptEnabled(true);
        WebAppInterface htmlInterface = new WebAppInterface(this);
        mappa.addJavascriptInterface(htmlInterface,"Mappa");
    }

    private class WebAppInterface {
        Context mContext;

        WebAppInterface(Context c) {
            mContext = c;
        }

        @JavascriptInterface
        public void goToMap() {
            intent=new Intent(UserActivity.this,MappaActivity.class);
            startActivity(intent);
        }
    }


    @SuppressLint("SetTextI18n")
    private void settaTesto(TextView email, TextView telefono, TextView indirizzo){
        data=UserData.getInstance();
        email.setText("Indirizzo e-mail: "+data.info().getMail());
        telefono.setText("Telefono: "+data.info().getTelefono());
        indirizzo.setText("Residenza: "+data.info().getIndirizzo());
    }

}
