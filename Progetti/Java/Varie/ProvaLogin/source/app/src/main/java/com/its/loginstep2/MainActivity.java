package com.its.loginstep2;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.github.ybq.android.spinkit.SpinKitView;

/**
 * @author Simone Tugnetti
 * Simulazione di un login con Registrazione e visualizzazione dei propri dati,
 * salvataggio degli stessi su un Singleton, WebView, Maps (Inserire API) e ViewPager
 */
public class MainActivity extends AppCompatActivity {

    private EditText mail,password;
    private Button login,registrati;
    private TextView domanda;
    private View oscurita;
    private SpinKitView caricamento;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mail = findViewById(R.id.mail);
        password = findViewById(R.id.password);
        login = findViewById(R.id.btnLogin);
        oscurita = findViewById(R.id.oscurita);
        caricamento = findViewById(R.id.caricamento);
        registrati = findViewById(R.id.registrati);
        domanda = findViewById(R.id.domanda);

        mail.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                mail.setHintTextColor(ContextCompat.getColor(MainActivity.this,R.color.colorHint));
                mail.setHint("Inserisci la tua e-mail");
            }
        });

        password.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                password.setHintTextColor(ContextCompat.getColor(MainActivity.this,
                        R.color.colorHint));
                password.setHint("Inserisci la password");
            }
        });

        registrati.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registratiActivity();
            }
        });

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mailOPassMancanti(mail,password)) {
                    erroreMailPassord(mail,password);
                } else if (mailValida(mail)) {
                    alert("Attenzione","Formato e-mail non vaildo!");
                    errore(mail,"Formato e-mail non valido");
                    password.setText("");
                }else{
                    esito();
                }
            }
        });

    }


    private void registratiActivity(){
        Intent intent = new Intent(this,RegistratiActivity.class);
        startActivity(intent);
    }

    private void esito(){
        disattiva();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                attiva();
                if(verifica(mail,password)){
                    alertInvio("Successo","Login avvenuto con successo");
                }else{
                    mail.setText("");
                    password.setText("");
                    alert("Attenzione","Indirizzo e-mail o password errati");
                }
            }
        },3000);
    }

    private void alertInvio(String titolo, String msg){
        new AlertDialog.Builder(this).setTitle(titolo).setMessage(msg)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                userActivity();
            }
        }).show();
    }

    private void attiva(){
        caricamento.setVisibility(View.INVISIBLE);
        oscurita.setVisibility(View.INVISIBLE);
        login.setVisibility(View.VISIBLE);
        registrati.setVisibility(View.VISIBLE);
        mail.setEnabled(true);
        password.setEnabled(true);
        domanda.setVisibility(View.VISIBLE);
    }

    private boolean verifica(EditText mail, EditText password){
        UserData data=UserData.getInstance();
        return mail.getText().toString().equals(data.info().getMail()) &&
                password.getText().toString().equals(data.info().getPassword());
    }

    private void userActivity(){
        Intent intent=new Intent(this,UserActivity.class);
        startActivity(intent);
    }

    private void disattiva(){
        caricamento.setVisibility(View.VISIBLE);
        oscurita.setVisibility(View.VISIBLE);
        login.setVisibility(View.INVISIBLE);
        registrati.setVisibility(View.INVISIBLE);
        mail.setEnabled(false);
        password.setEnabled(false);
        domanda.setVisibility(View.INVISIBLE);
    }

    private void alert(String titolo, String msg){
        new AlertDialog.Builder(this).setTitle(titolo).setMessage(msg)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        }).show();
    }

    private void erroreMailPassord(EditText mail, EditText password){
        if(mail.getText().toString().equals("")){
            errore(mail,"Indirizzo e-mail mancante");
        }
        if(password.getText().toString().equals("")){
            errore(password,"Password mancante");
        }
    }

    private void errore(EditText testo, String msg){
        testo.setText("");
        testo.setHintTextColor(ContextCompat.getColor(this,R.color.errore));
        testo.setHint(msg);
    }

    private boolean mailValida(EditText mail){
        return !Patterns.EMAIL_ADDRESS.matcher(mail.getText().toString()).matches();
    }

    private boolean mailOPassMancanti(EditText mail, EditText password){
        return mail.getText().toString().equals("") || password.getText().toString().equals("");
    }

}
