package com.its.loginstep2;

import com.google.android.material.tabs.TabLayout;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

/**
 * @author Simone Tugnetti
 */
public class ModificaActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modifica);
        ViewPager viewPager = findViewById(R.id.contenuti);
        TabLayout tabLayout = findViewById(R.id.navigation);

        viewPager.setOffscreenPageLimit(2);

        GestioneFragment gestioneFragment = new GestioneFragment(getSupportFragmentManager());

        viewPager.setAdapter(gestioneFragment);

        tabLayout.setupWithViewPager(viewPager);


    }

}
