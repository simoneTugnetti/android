package com.its.loginstep2;


import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Objects;


/**
 * @author Simone Tugnetti
 */
public class ContattoFragment extends Fragment {

    private ConstraintLayout csLayoutCompleto;

    public ContattoFragment() {
        // Required empty public constructor§
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_contatto, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        TextView info = Objects.requireNonNull(getView()).findViewById(R.id.informazioni);
        ConstraintLayout csLayoutDettaglio = getView().findViewById(R.id.dettaglio);
        csLayoutCompleto=getView().findViewById(R.id.completo);

        UserData data= UserData.getInstance();
        info.setText(data.info().toString());

        csLayoutDettaglio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                csLayoutCompleto.setVisibility(View.VISIBLE);
                assert getFragmentManager() != null;
                FragmentTransaction fragment = getFragmentManager().beginTransaction();
                fragment.setCustomAnimations(R.anim.slide_in_enter_micro,
                        R.anim.slide_in_exit_micro,R.anim.slide_in_up,R.anim.slide_out_down);
                fragment.replace(R.id.completo, new CompletoFragment());
                fragment.addToBackStack(null);
                fragment.commit();
            }
        });


    }
}
