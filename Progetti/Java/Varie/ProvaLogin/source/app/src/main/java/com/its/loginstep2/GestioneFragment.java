package com.its.loginstep2;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

/**
 * @author Simone Tugnetti
 */
public class GestioneFragment extends FragmentStatePagerAdapter {


    GestioneFragment(FragmentManager fm) {
        super(fm, FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
    }

    @SuppressWarnings("NullableProblems")
    @Override
    public Fragment getItem(int i) {

        if (i == 1) {
            return new AccountFragment();
        }
        return new InfoFragment();
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position){

        switch(position){
            case 0:
                return "Info";
            case 1:
                return "Account";
            default:
                return null;
        }

    }
}
