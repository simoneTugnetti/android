package com.its.loginstep2;

/**
 * @author Simone Tugnetti
 */
public class User {

    private String nome,cognome,mail,password,telefono,indirizzo;

    User() {
    }

    String getIndirizzo() {
        return indirizzo;
    }

    void setIndirizzo(String indirizzo) {
        this.indirizzo = indirizzo;
    }

    String getTelefono() {
        return telefono;
    }

    void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    String getPassword() {
        return password;
    }

    void setPassword(String password) {
        this.password = password;
    }

    String getMail() {
        return mail;
    }

    void setMail(String mail) {
        this.mail = mail;
    }

    public String getCognome() {
        return cognome;
    }

    void setCognome(String cognome) {
        this.cognome = cognome;
    }

    public String getNome() {
        return nome;
    }

    void setNome(String nome) {
        this.nome = nome;
    }

    @Override
    public String toString() {
        return "Dati Utente\n\n" +
                "Nome: " + nome + "\n" +
                "Cognome: " + cognome + "\n" +
                "Indirizzo e-mail: " + mail + '\n' +
                "Telefono: " + telefono+'\n'+
                "Residenza: "+indirizzo;
    }
}
