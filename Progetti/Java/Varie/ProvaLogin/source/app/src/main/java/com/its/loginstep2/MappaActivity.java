package com.its.loginstep2;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import androidx.fragment.app.FragmentActivity;
import android.os.Bundle;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

/**
 * @author Simone Tugnetti
 */
public class MappaActivity extends FragmentActivity implements OnMapReadyCallback,
        GoogleMap.OnMarkerClickListener {

    private Marker locate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mappa);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        assert mapFragment != null;
        mapFragment.getMapAsync(this);
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        googleMap.setOnMarkerClickListener(this);
        UserData data = UserData.getInstance();
        Geocoder geo = new Geocoder(this, Locale.getDefault());
        try {
            List<Address> indirizzi = geo.getFromLocationName(data.info().getIndirizzo(), 1);
            LatLng indirizzo = new LatLng(indirizzi.get(0).getLatitude(),
                    indirizzi.get(0).getLongitude());
            BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(R.drawable.ic_location_pin);
            locate = googleMap.addMarker(new MarkerOptions().position(indirizzo)
                    .title("ITS - ICT Piemonte").icon(icon));
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(indirizzo, 15.5F));
        } catch (IOException e) {
            new AlertDialog.Builder(this).setTitle("Attenzione")
                    .setMessage("Si è verificato il seguente errore: \n" + e.getMessage())
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent intent = new Intent(MappaActivity.this,
                                    UserActivity.class);
                            startActivity(intent);
                        }
                    }).show();
        }

    }

    @Override
    public boolean onMarkerClick(Marker marker) {

        if (marker.equals(locate)) {
            FragmentTransaction fragment = getSupportFragmentManager().beginTransaction();
            fragment.add(R.id.principale, new ContattoFragment());
            fragment.addToBackStack(null);
            fragment.commit();
        }

        return false;
    }
}
