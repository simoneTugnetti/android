package com.its.loginstep2;


import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import java.util.Objects;


/**
 * @author Simone Tugnetti
 */
public class AccountFragment extends Fragment {

    private EditText pswAttuale,pswNuova,pswConferma;
    private UserData data;


    public AccountFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_account, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        pswAttuale= Objects.requireNonNull(getView()).findViewById(R.id.pswAttuale);
        pswNuova=getView().findViewById(R.id.pswNuova);
        pswConferma=getView().findViewById(R.id.pswConferma);
        Button conferma = getView().findViewById(R.id.conferma);

        pswAttuale.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                resetHint(pswAttuale,"Inserisci la password attuale");
            }
        });

        pswNuova.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                resetHint(pswNuova,"Inserisci la nuova password");
            }
        });

        pswConferma.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                resetHint(pswConferma,"Conferma la password");
            }
        });

        conferma.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (elemeMancanti(pswAttuale,pswNuova,pswConferma)) {
                    erroreElem(pswAttuale, pswNuova, pswConferma);
                }else if(pswInesistente(pswAttuale)){
                    alert("Attenzione","Password attuale inesistente!");
                    errore(pswAttuale,"Inserisci la password attuale");
                    errore(pswNuova,"Inserisci la nuova password");
                    errore(pswConferma,"Conferma la password");
                }else if(pswAttualeUNuova(pswAttuale,pswNuova)) {
                    alert("Attenzione", "La nuova password deve essere differente da quella attuale!");
                    errore(pswAttuale, "Inserisci la password attuale");
                    errore(pswNuova, "Inserisci la nuova password");
                    errore(pswConferma, "Conferma la password");
                }else if(passSupSei(pswNuova)){
                    alert("Attenzione","La password deve contenere almeno sei caratteri!");
                    errore(pswNuova,"Formato password non valido");
                    errore(pswConferma,"Reinserisci la password");
                }else if(coincidaPass(pswNuova,pswConferma)){
                    alert("Attenzione","Le password non coincidono!");
                    errore(pswConferma,"Reinserisci la password");
                }else{
                    alertInvio("Successo","Password modificata con successo!");
                }
            }
        });

    }

    private void alertInvio(String titolo, String msg){
        new AlertDialog.Builder(Objects.requireNonNull(getContext())).setTitle(titolo)
                .setMessage(msg).setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                successo();
            }
        }).show();
    }

    private void successo(){
        data=UserData.getInstance();
        data.info().setPassword(pswNuova.getText().toString());
        Intent intent=new Intent(Objects.requireNonNull(getActivity()).getApplication(),
                MainActivity.class);
        startActivity(intent);
    }

    private void resetHint(EditText testo, String msg){
        testo.setHintTextColor(ContextCompat.getColor(Objects.requireNonNull(getContext()),
                R.color.colorHint));
        testo.setHint(msg);
    }

    private void alert(String titolo, String msg){
        new AlertDialog.Builder(Objects.requireNonNull(getContext())).setTitle(titolo)
                .setMessage(msg).setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        }).show();
    }

    private void erroreElem(EditText pswAttuale, EditText pswNuova, EditText pswConferma){
        if(pswAttuale.getText().toString().equals("")){
            errore(pswAttuale,"Password attuale mancante");
        }
        if(pswNuova.getText().toString().equals("")){
            errore(pswNuova,"Nuova password mancante");
        }
        if(pswConferma.getText().toString().equals("")){
            errore(pswConferma,"Conferma passowrd mancante");
        }
    }

    private boolean pswInesistente(EditText pswAttuale){
        data=UserData.getInstance();
        return !pswAttuale.getText().toString().equals(data.info().getPassword());
    }

    private boolean pswAttualeUNuova(EditText pswAttuale,EditText pswNuova){
        return pswAttuale.getText().toString().equals(pswNuova.getText().toString());
    }

    private boolean coincidaPass(EditText pswNuova, EditText pswConferma){
        return !pswNuova.getText().toString().equals(pswConferma.getText().toString());
    }

    private boolean passSupSei(EditText password){
        return password.getText().length()<6;
    }

    private boolean elemeMancanti(EditText pswAttuale, EditText pswNuova, EditText pswConferma){
        return pswAttuale.getText().toString().equals("") ||
                pswNuova.getText().toString().equals("") ||
                pswConferma.getText().toString().equals("");
    }

    private void errore(EditText testo, String msg){
        testo.setText("");
        testo.setHintTextColor(ContextCompat.getColor(Objects.requireNonNull(getContext()),
                R.color.errore));
        testo.setHint(msg);
    }


}