package com.its.adapter1;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

/**
 * @author Simone Tugnetti
 * Utilizzo base di una ListView
 */
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ListView listView = findViewById(R.id.lista);

        String[] nomi={"Aldo","Giovanni","Giacomo"};

        //L'adapter vuole in ingresso tre parametri:
        //- context (this)
        //- il layout della singola cella
        //- l'array di valori che devono essere stampati nella lista
        ArrayAdapter<String> adapter=new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1,nomi);
        listView.setAdapter(adapter);
    }
}
