package it.consoft.provanfc2;

import android.nfc.FormatException;
import android.nfc.NdefMessage;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.nfc.tech.NdefFormatable;
import android.util.Log;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class WritableTag {

    private final Ndef ndef;
    private final NdefFormatable ndefFormatable;

    public WritableTag(Tag tag) throws FormatException {
        String[] technologies = tag.getTechList();
        List<String> tagTechs = Arrays.asList(technologies.clone());
        String NDEF = Ndef.class.getCanonicalName();
        String NDEF_FORMATABLE = NdefFormatable.class.getCanonicalName();
        if (tagTechs.contains(NDEF)) {
            Log.i("WritableTag", "contains ndef");
            ndef = Ndef.get(tag);
            ndefFormatable = null;
        } else if (tagTechs.contains(NDEF_FORMATABLE)) {
            ndefFormatable = NdefFormatable.get(tag);
            ndef = null;
        } else throw new FormatException("Tag doesn't support ndef");

    }

    public String getTagId() {
        if (ndef != null) return bytesToHexString(ndef.getTag().getId());
        else if (ndefFormatable != null) return bytesToHexString(ndefFormatable.getTag().getId());
        return null;
    }

    public String[] getTagTechList() {
        if (ndef != null) return ndef.getTag().getTechList();
        else if (ndefFormatable != null) return ndefFormatable.getTag().getTechList();
        return null;
    }

    public Boolean writeData(String tagId, NdefMessage message) throws IOException, FormatException {
        if (!getTagId().equals(tagId)) return false;
        if (ndef != null) {
            ndef.connect();
            if (ndef.isConnected()) {
                ndef.writeNdefMessage(message);
                return true;
            }
        } else if (ndefFormatable != null) {
            ndefFormatable.connect();
            if (ndefFormatable.isConnected()) {
                ndefFormatable.format(message);
                return true;
            }
        }
        return false;
    }

    private void close() throws IOException {
        if (ndef != null) ndef.close();
        else if (ndefFormatable != null) ndefFormatable.close();
    }

    public static String bytesToHexString(byte[] src) {
        if (ByteUtils.isNullOrEmpty(src)) return null;
        StringBuilder sb = new StringBuilder();
        for (byte b : src) {
            sb.append(String.format("%02X", b));
        }
        return sb.toString();
    }

}
