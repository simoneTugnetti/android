package it.consoft.provanfc2;

import androidx.appcompat.app.AppCompatActivity;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.nfc.FormatException;
import android.nfc.NfcAdapter;
import android.nfc.NfcManager;
import android.nfc.Tag;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;

public class MainActivity extends AppCompatActivity {

    private NfcAdapter nfcAdapter = null;
    WritableTag tag = null;
    String tagId = null;

    Button activeTag;
    TextView textUid;
    TextView textData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        nfcAdapter = ((NfcManager) getSystemService(Context.NFC_SERVICE)).getDefaultAdapter();

        activeTag = findViewById(R.id.button);
        textUid = findViewById(R.id.textView);
        textData = findViewById(R.id.textView2);

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        Tag tagFromIntent = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
        try {
            tag = new WritableTag(tagFromIntent);
            tagId = tag.getTagId();
            onTagTapped(tagId, Arrays.toString(tag.getTagTechList()));
            showToast("Tag tapped: " + tagId);
        } catch (FormatException e) {
            Log.e(getTag(), "Unsupported tag tapped", e);
        }

        if (NfcAdapter.ACTION_NDEF_DISCOVERED.equals(intent.getAction())) {
            ArrayList<Parcelable> rawMsg = intent.getParcelableArrayListExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
            if (rawMsg != null) onTagTapped(NfcUtils.getUID(intent), NfcUtils.getData(rawMsg));
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        enableNfcForeground();
    }

    @Override
    protected void onPause() {
        super.onPause();
        disableNfcForeground();
    }

    private String getTag() {
        return "Main Activity";
    }

    private void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    private void disableNfcForeground() {
        try {
            nfcAdapter.disableForegroundDispatch(this);
        } catch (IllegalStateException e) {
            Log.e(getTag(), "Error disabling NFC", e);
        }
    }

    private void enableNfcForeground() {
        try {
            Intent intent = new Intent(this, getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            PendingIntent nfcPendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_IMMUTABLE);
            nfcAdapter.enableForegroundDispatch(this, nfcPendingIntent, null, null);
        } catch (IllegalStateException e) {
            Log.e(getTag(), "Error enabling NFC", e);
        }
    }

    private void onTagTapped(String superTagId, String superTagData) {
        textUid.setText(superTagId);
        textData.setText(superTagData);
    }

}