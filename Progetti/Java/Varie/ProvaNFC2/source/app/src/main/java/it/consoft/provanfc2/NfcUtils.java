package it.consoft.provanfc2;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.os.Parcelable;
import android.util.Log;

import com.google.common.io.BaseEncoding;

import java.util.ArrayList;

public class NfcUtils {

    private static final String mimeType = "";

    static String getUID(Intent intent) {
        Tag myTag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
        return BaseEncoding.base16().encode(myTag.getId());
    }

    static String getData(ArrayList<Parcelable> rawMsg) {
        ArrayList<NdefMessage> msgs = new ArrayList<>(rawMsg.size());

        for (int i = 0; i < rawMsg.size(); i++) msgs.set(i, (NdefMessage) rawMsg.get(i));

        NdefRecord[] records = msgs.get(0).getRecords();

        StringBuilder recordData = new StringBuilder();

        for (NdefRecord record : records) {
            recordData.append(record.toString()).append("\n");
        }

        return recordData.toString();
    }

    static ArrayList<IntentFilter> getIntentFilters() {
        IntentFilter ndefFilter = new IntentFilter(NfcAdapter.ACTION_NDEF_DISCOVERED);

        try {
            ndefFilter.addDataType("text/plain");
        } catch (IntentFilter.MalformedMimeTypeException e) {
            Log.e("NfcUtils", "Problem in parsing mime type for nfc reading", e);
        }

        ArrayList<IntentFilter> ndefArrayListIntent = new ArrayList<>();

        ndefArrayListIntent.add(ndefFilter);

        return ndefArrayListIntent;
    }

    static NdefMessage prepareMessageToWrite(String tagData, Context context) {
        NdefMessage message;
        byte[] typeBytes = mimeType.getBytes();
        byte[] payload = tagData.getBytes();
        NdefRecord record1 = new NdefRecord(NdefRecord.TNF_MIME_MEDIA, typeBytes, null, payload);
        NdefRecord record2 = NdefRecord.createApplicationRecord(context.getPackageName());

        ArrayList<NdefRecord> arrayRecords = new ArrayList<>();

        arrayRecords.add(record1);
        arrayRecords.add(record2);

        message = new NdefMessage((NdefRecord[]) arrayRecords.toArray());

        return message;
    }

}
