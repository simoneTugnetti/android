package it.consoft.provanfc2;

public class StringUtils {

    static String randomString(int length) {
        String chars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        StringBuilder str = new StringBuilder();
        for (int i = 0; i < length; i++) {
            str.append(chars.charAt((int) Math.floor(Math.random() * chars.length())));
        }
        return str.toString();
    }

}
