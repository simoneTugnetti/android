package com.its.activityintent2;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.widget.ImageView;

import java.io.InputStream;
import java.net.URL;
import java.util.Objects;
import java.util.Random;

/**
 * @author Simone Tugnetti
 */
public class ImmActivity extends AppCompatActivity {

    private String[] immagini;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_immagini);
        ImageView sfondo = findViewById(R.id.sfondo);
        inizImmagini();
        new DownloadImage(sfondo).execute(immagini[numeriRandom()]);
    }

    private int numeriRandom(){
        Random r=new Random();
        return r.nextInt(4);
    }

    private void inizImmagini(){
        immagini=new String[5];
        immagini[0]="https://img2.tgcom24.mediaset.it/binary/fotogallery/istockphoto/41.$plit/C_2_" +
                "fotogallery_3084643_9_image.jpg?20171215053032";
        immagini[1]="http://www.provincia.mc.it/wp-content/blogs.dir/1/files/2013/07/paesaggi_natura_monti_sibillini1.jpg";
        immagini[2]="http://dam.boscolo.com/static/passion_Paesaggi-italiani_690x385.jpg";
        immagini[3]="https://www.napolitan.it/wp-content/uploads/2017/10/ssimg_235-1024x683.jpg";
        immagini[4]="https://www.skyscanner.it/wp-content/uploads/2018/05/GettyImages-484569884.jpg?fit=1048,699";
    }

    private static class DownloadImage extends AsyncTask<String,Void,Bitmap>{

        @SuppressLint("StaticFieldLeak")
        private ImageView image;

        @SuppressWarnings("deprecation")
        DownloadImage(ImageView image){
            this.image=image;
        }

        @Override
        protected Bitmap doInBackground(String... url) {
            Bitmap immagine=null;
            try {
                InputStream in = new URL(url[0]).openStream();
                immagine = BitmapFactory.decodeStream(in);
            }catch(Exception e){
                Log.e("Errore", Objects.requireNonNull(e.getMessage()));
            }
            return immagine;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
            image.setImageBitmap(bitmap);
        }
    }

}
