package com.its.activityintent2;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

/**
 * @author Simone Tugnetti
 * Visualizza un'immagine random da visualizzare
 */
public class MainActivity extends AppCompatActivity {

    Button avvia;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        avvia=findViewById(R.id.btnAvvia);

        avvia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                immCasuali();
            }
        });
    }

    private void immCasuali(){
        Intent intent = new Intent(MainActivity.this, ImmActivity.class);
        startActivity(intent);
    }
}
