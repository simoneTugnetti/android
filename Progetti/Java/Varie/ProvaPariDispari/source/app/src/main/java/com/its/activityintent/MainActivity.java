package com.its.activityintent;

import android.content.Intent;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

/**
 * @author Simone Tugnetti
 * Verifica Pari o dispari di un numeero in input
 */
public class MainActivity extends AppCompatActivity {

    private EditText valore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        valore = findViewById(R.id.txtValore);
        Button verifica = findViewById(R.id.btnVerifica);

        verifica.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(valore.getText().toString().equals("")){
                    avviso();
                }else {
                    if ((Integer.parseInt(valore.getText().toString())) % 2 == 0) {
                        esitoActivity(R.color.pari, "Il valore è pari");
                    } else {
                        esitoActivity(R.color.dispari, "Il valore è dispari");
                    }
                }
            }
        });
    }

    private void avviso(){
        valore.setHint("Inserisci un valore!");
        valore.setHintTextColor(ContextCompat.getColor(MainActivity.this,R.color.errore));
    }

    private void esitoActivity(int colore, String testo){
        //Utilizzato per lo scambio tra activity
        Intent intent = new Intent(MainActivity.this, EsitoActivity.class);
        intent.putExtra("colore",colore);
        intent.putExtra("testo",testo);
        startActivity(intent);
    }

}
