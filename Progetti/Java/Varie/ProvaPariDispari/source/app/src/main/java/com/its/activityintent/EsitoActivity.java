package com.its.activityintent;

import android.content.Intent;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

/**
 * @author Simone Tugnetti
 */
public class EsitoActivity extends AppCompatActivity {
    View sfondo;
    TextView esito;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_esito);
        sfondo = findViewById(R.id.sfondo);
        esito = findViewById(R.id.esito);

        Intent intent=getIntent();
        sfondo.setBackgroundColor(ContextCompat.getColor(this,
                intent.getIntExtra("colore",0)));
        esito.setText(intent.getStringExtra("testo"));
    }
}
