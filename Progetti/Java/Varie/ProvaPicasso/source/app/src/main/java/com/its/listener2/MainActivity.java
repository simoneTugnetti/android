package com.its.listener2;

import android.annotation.SuppressLint;
import android.content.DialogInterface;

import com.google.android.material.snackbar.Snackbar;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

/**
 * @author Simone Tugnetti
 * Verifica se un numero e' maggiore o compreso tra 0 e 10, modificando il testo di conseguenza
 */
public class MainActivity extends AppCompatActivity {

    private EditText testo;
    private TextView esito;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        testo=findViewById(R.id.testo);
        Button controllo = findViewById(R.id.controlla);
        esito=findViewById(R.id.esito);
        ImageView background = findViewById(R.id.background);

        controllo.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View v) {
                try {
                    int valore = Integer.parseInt(testo.getText().toString());
                    if (valore >= 0 && valore <= 10) {
                        esito.setText("OK");
                        esito.setTextColor(ContextCompat.getColor(MainActivity.this,
                                R.color.verde));
                    } else if (valore > 10) {
                        esito.setText("ERROR");
                        esito.setTextColor(ContextCompat.getColor(MainActivity.this,
                                R.color.rosso));
                    }
                }catch(NumberFormatException ignored){

                }
            }
        });
        Picasso.get().load("https://i.imgur.com/DvpvklR.png").into(background);

        Snackbar.make(getWindow().getDecorView(),"Testo da mostrare",Snackbar.LENGTH_LONG).show();

        new AlertDialog.Builder(this).setTitle("Attenzione!").setMessage("Alert di Default")
                .setPositiveButton("OK", null)
        .setNegativeButton("Annulla", null)
        .show();

    }

    /*
    Nel bottone:
    android:onclick="calcola"

    In MainActivity.java:
    public void calcola(View v){

    }
    */
}
