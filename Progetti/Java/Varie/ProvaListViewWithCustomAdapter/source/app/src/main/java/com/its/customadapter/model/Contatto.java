package com.its.customadapter.model;

/**
 * @author Simone Tugnetti
 */
public class Contatto {

    public String name;
    public String surname;
    public String phone;
    public int image;

    public Contatto(String name, String surname, String phone, int image){
        this.name=name;
        this.surname=surname;
        this.phone=phone;
        this.image=image;
    }

}
