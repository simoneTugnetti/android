package com.its.customadapter;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import com.its.customadapter.adapter.CustomAdapter;
import com.its.customadapter.model.Contatto;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Simone Tugnetti
 * Visualizzazione di una lista usando ListView custom Adapter
 */
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ListView listView = findViewById(R.id.lista);

        List<Contatto> contacts = new ArrayList<>();

        Contatto p1 = new Contatto("Mario","Rossi","333",R.drawable.rick);
        Contatto p2 = new Contatto("Luigi","Verdi","555",R.drawable.aang);
        contacts.add(p1);
        contacts.add(p2);

        //Definisco l'adapter
        CustomAdapter adapter = new CustomAdapter(this,R.layout.custom_row,contacts);
        listView.setAdapter(adapter);

    }
}
