package com.its.customadapter.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import androidx.core.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.its.customadapter.R;
import com.its.customadapter.model.Contatto;

import java.util.List;

/**
 * @author Simone Tugnetti
 */
public class CustomAdapter extends ArrayAdapter {

    private Context mContext;
    private int mLayoutID;

    //Costruttore
    @SuppressWarnings("unchecked")
    public CustomAdapter(Context context, int layoutID, List<Contatto> valori){
        super(context,layoutID,valori);
        mContext = context;
        mLayoutID = layoutID;
    }

    //Designo la singola cella
    @SuppressWarnings("NullableProblems")
    @SuppressLint({"ViewHolder", "SetTextI18n"})
    public View getView(int position, View view, ViewGroup parentView){

        //Una classe che permette di prendere una view ed inserirla all'interno di un'altra view
        LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        //Prendi l'XML e inseriscilo dentro la parentView (attach false --> non lo disegna a video)
        assert layoutInflater != null;
        view=layoutInflater.inflate(mLayoutID,parentView,false);

        TextView nameText = view.findViewById(R.id.nameText);
        TextView surnameText = view.findViewById(R.id.surnameText);
        TextView phoneText = view.findViewById(R.id.phoneText);
        ImageView imageView = view.findViewById(R.id.immagine);

        Contatto contatto=(Contatto) getItem(position);
        assert contatto != null;
        nameText.setText("     "+contatto.name);
        surnameText.setText("     "+contatto.surname);
        phoneText.setText("     "+contatto.phone);
        imageView.setImageDrawable(ContextCompat.getDrawable(mContext,contatto.image));

        return view;
    }

}
