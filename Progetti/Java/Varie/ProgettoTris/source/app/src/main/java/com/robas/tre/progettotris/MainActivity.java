package com.robas.tre.progettotris;

import android.annotation.SuppressLint;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Arrays;
import java.util.List;

/**
 * @author Simone Tugnetti
 * Primo Progetto Intermedio -> Tic-Tac-Toe
 */
public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    int giocatore, simbolo, vincitaO = 0, vincitaX = 0;
    TextView testo, textRisultato;
    Button nuova, risultato;
    ImageButton ib1, ib2, ib3, ib4, ib5, ib6, ib7, ib8, ib9;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        testo = findViewById(R.id.testo);
        nuova = findViewById(R.id.nuovo);
        textRisultato = findViewById(R.id.textRisultato);
        risultato = findViewById(R.id.risultato);
        ib1 = findViewById(R.id.imageButton);
        ib2 = findViewById(R.id.imageButton2);
        ib3 = findViewById(R.id.imageButton3);
        ib4 = findViewById(R.id.imageButton4);
        ib5 = findViewById(R.id.imageButton5);
        ib6 = findViewById(R.id.imageButton6);
        ib7 = findViewById(R.id.imageButton7);
        ib8 = findViewById(R.id.imageButton8);
        ib9 = findViewById(R.id.imageButton9);
        ib1.setOnClickListener(this);
        ib2.setOnClickListener(this);
        ib3.setOnClickListener(this);
        ib4.setOnClickListener(this);
        ib5.setOnClickListener(this);
        ib6.setOnClickListener(this);
        ib7.setOnClickListener(this);
        ib8.setOnClickListener(this);
        ib9.setOnClickListener(this);
        nuova.setOnClickListener(this);
        risultato.setOnClickListener(this);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onClick(View v){

        if(giocatore == 0){
            simbolo = R.drawable.o1;
        } else{
            simbolo = R.drawable.redx;
        }

        switch (v.getId()) {
            case R.id.imageButton:
                setImage(ib1);
                break;
            case R.id.imageButton2:
                setImage(ib2);
                break;
            case R.id.imageButton3:
                setImage(ib3);
                break;
            case R.id.imageButton4:
                setImage(ib4);
                break;
            case R.id.imageButton5:
                setImage(ib5);
                break;
            case R.id.imageButton6:
                setImage(ib6);
                break;
            case R.id.imageButton7:
                setImage(ib7);
                break;
            case R.id.imageButton8:
                setImage(ib8);
                break;
            case R.id.imageButton9:
                setImage(ib9);
                break;
            case R.id.nuovo:
                reset(Arrays.<ImageView>asList(ib1, ib2, ib3, ib4, ib5, ib6, ib7, ib8, ib9));
                testo.setText("");
                break;
            case R.id.risultato:
                textRisultato.setText("O: " + vincitaO + "\n" + "X: " + vincitaX);
                break;

        }
    }

    /**
     * Viene settata l'immagine in base al turno del giocatore, cioè 0 o 1
     * @param image ImageView da modificare
     */
    void setImage(ImageView image) {
        image.setImageResource(simbolo);
        image.setTag(simbolo);
        giocatore = 1 - giocatore;
        image.setEnabled(false);
        combination();
    }

    /**
     * Resetta l'intera griglia
     * @param images Lista di ImageView da resettare
     */
    void reset(List<ImageView> images) {
        for (ImageView image: images){
            image.setImageResource(R.drawable.bianco);
            image.setTag(null);
            image.setEnabled(true);
        }
    }

    void combination(){
        getCombination(ib1, ib3, ib4, simbolo);
        getCombination(ib2, ib5, ib6, simbolo);
        getCombination(ib7, ib8, ib9, simbolo);
        getCombination(ib1, ib2, ib7, simbolo);
        getCombination(ib3, ib5, ib8, simbolo);
        getCombination(ib4, ib6, ib9, simbolo);
        getCombination(ib1, ib5, ib9, simbolo);
        getCombination(ib4, ib5, ib7, simbolo);
    }

    /**
     * Se la combinazione di ImageView possiedono lo stesso simbolo, la TextView inerente
     * alla vincita segnerà che avrà vinto il giocatore con il simbolo corrispondente,
     * bloccando tutte le caselle e incrementando la variabile del simbolo vincitore
     * @param first prima ImageView
     * @param second seconda ImageView
     * @param third terza ImageView
     * @param sign il simbolo da controllare
     */
    @SuppressLint("SetTextI18n")
    void getCombination(ImageView first, ImageView second, ImageView third, int sign) {
        if(first.getTag() != null && second.getTag() != null && third.getTag() != null) {
            if (first.getTag().equals(sign) && second.getTag().equals(sign)
                    && third.getTag().equals(sign)) {

                if(sign == R.drawable.o1) {
                    testo.setText("Ha vinto la O");
                    vincitaO = vincitaO + 1;
                }else {
                    testo.setText("Ha vinto la X");
                    vincitaX = vincitaX + 1;
                }

                disableAllImage(Arrays
                        .<ImageView>asList(ib1, ib2, ib3, ib4, ib5, ib6, ib7, ib8, ib9));
            }
        }
    }

    /**
     * Disabilita tutte le caselle
     * @param images lista immagini
     */
    void disableAllImage(List<ImageView> images) {
        for (ImageView image: images) {
            image.setEnabled(false);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main_tris, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
