package com.its.viewpagerexample;

import com.google.android.material.tabs.TabLayout;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

/**
 * @author Simone Tugnetti
 * Prova di utilizzo di ViewPager
 */
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Istanzio il viewPager
        ViewPager viewPager = findViewById(R.id.viewPager);
        viewPager.setOffscreenPageLimit(4);

        //Istanzio l'adapter
        CustomPageAdapter adapter = new CustomPageAdapter(getSupportFragmentManager());

        //Assegno l'adapter al ViewPager
        viewPager.setAdapter(adapter);

        //Istanzio il TabLayout
        TabLayout tabs = findViewById(R.id.tabs);

        //Associare il TabLayout al ViewPager
        tabs.setupWithViewPager(viewPager);

    }
}
