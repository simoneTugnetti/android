package com.its.viewpagerexample;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.its.viewpagerexample.page.AboutFragment;
import com.its.viewpagerexample.page.ContactFragment;
import com.its.viewpagerexample.page.InfoFragment;
import com.its.viewpagerexample.page.PageFragment;

/**
 * @author Simone Tugnetti
 */
public class CustomPageAdapter extends FragmentStatePagerAdapter {

    CustomPageAdapter(FragmentManager fm) {
        super(fm, FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
    }


    //Definire il fragment che comporrà la singola pagina
    @NonNull
    @Override
    public Fragment getItem(int i) {

        switch (i) {
            case 1:
                return new InfoFragment();
            case 2:
                return new AboutFragment();
            case 3:
                return new ContactFragment();
            default:
                return new PageFragment();
        }

    }


    //Numero di pagine
    @Override
    public int getCount() {
        return 4;
    }


    //Ritorna il titolo della pagina
    @Override
    public CharSequence getPageTitle(int position){

        switch (position){
            case 0:
                return "Page";
            case 1:
                return "Info";
            case 2:
                return "About";
            case 3:
                return "Conatct";
            default:
                return null;
        }

    }

}
