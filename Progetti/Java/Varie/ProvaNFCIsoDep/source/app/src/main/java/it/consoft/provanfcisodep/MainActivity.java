package it.consoft.provanfcisodep;

import androidx.appcompat.app.AppCompatActivity;

import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.FormatException;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.IsoDep;
import android.nfc.tech.NdefFormatable;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    byte[] SELECT = {
            (byte) 0x00, // CLA Class
            (byte) 0xA4, // INS Instruction
            (byte) 0x04, // P1  Parameter 1
            (byte) 0x00, // P2  Parameter 2
            (byte) 0x0A, // Length
            0x63,0x64,0x63,0x00,0x00,0x00,0x00,0x32,0x32,0x31 // AID
    };

    private NfcAdapter nfcAdapter;
    private TextView info;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        nfcAdapter = NfcAdapter.getDefaultAdapter(this);

        info = findViewById(R.id.info);

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        if (intent.hasExtra(NfcAdapter.EXTRA_TAG)) {
            Toast.makeText(this, "NfcIntent!", Toast.LENGTH_SHORT).show();

            Parcelable[] parcelables = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);

            if (parcelables != null && parcelables.length > 0) {
                readTextFromMessage((NdefMessage) parcelables[0]);
            } else {
                Toast.makeText(this, "No NDEF message found", Toast.LENGTH_SHORT).show();
            }

//            Tag tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
//            NdefMessage ndefMessage = createNdefMessage("My String Content!");
        }

//        Tag tagFromIntent = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
//        IsoDep tag = IsoDep.get(tagFromIntent);
//
//        try {
//            tag.connect();
//            byte[] result = tag.transceive(SELECT);
//            int len = result.length;
//            byte[] data = new byte[len - 2];
//            System.arraycopy(result, 0, data, 0, len - 2);
//            String str = new String(data).trim();
//            info.setText(str);
//            tag.close();
//        } catch (IOException e) {
//            Toast.makeText(this, "", Toast.LENGTH_SHORT).show();
//        }

    }

    private void readTextFromMessage(NdefMessage ndefMessage) {
        NdefRecord[] ndefRecords = ndefMessage.getRecords();
        if (ndefRecords != null && ndefRecords.length > 0) {
            NdefRecord ndefRecord = ndefRecords[0];
            String tagContent = getTextFromNdefRecord(ndefRecord);
            info.setText(tagContent);
        } else {
            Toast.makeText(this, "No NDEF Record found", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        enableNfcForeground();
    }

    @Override
    protected void onStop() {
        super.onStop();
        disableNfcForeground();
    }

    private void formatTag(Tag tag, NdefMessage ndefMessage) {
        try {
            NdefFormatable ndefFormatable = NdefFormatable.get(tag);
            if (ndefFormatable == null) {
                Toast.makeText(this, "Tag is not Ndef Formatable", Toast.LENGTH_SHORT).show();
                return;
            }
            ndefFormatable.connect();
            ndefFormatable.format(ndefMessage);
            ndefFormatable.close();
        } catch (IOException | FormatException e) {
            Log.e("MainActivity", "Error Ndef Format", e);
        }
    }

    private String getTextFromNdefRecord(NdefRecord ndefRecord) {
        String tagContent = null;
        try {
            byte[] payload = ndefRecord.getPayload();
            String textEncoding = ((payload[0] & 128) == 0) ? "UTF-8" : "UTF-16";
            int languageSize = payload[0] & 0063;
            tagContent = new String(payload, languageSize + 1, payload.length - languageSize - 1, textEncoding);
        } catch (UnsupportedEncodingException e) {
            Log.e("getTextFromNdefRecord", e.getMessage(), e);
        }
        return tagContent;
    }

    private NdefMessage createNdefMessage(String content) {
        NdefRecord ndefRecord = NdefRecord.createTextRecord(Locale.getDefault().getLanguage(), content);
        return new NdefMessage(new NdefRecord[]{ ndefRecord });
    }

    private void disableNfcForeground() {
        try {
            nfcAdapter.disableForegroundDispatch(this);
        } catch (IllegalStateException e) {
            Log.e("MainActivity", "Error disabling NFC", e);
        }
    }

    private void enableNfcForeground() {
        try {
            Intent intent = new Intent(this, getClass()).addFlags(Intent.FLAG_RECEIVER_REPLACE_PENDING);
            PendingIntent nfcPendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_IMMUTABLE);
            nfcAdapter.enableForegroundDispatch(this, nfcPendingIntent, null, null);
        } catch (IllegalStateException e) {
            Log.e("MainActivity", "Error enabling NFC", e);
        }
    }

}