package com.its.constraint;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

/**
 * @author Simone Tugnetti
 * Somma di due valori in input
 */
public class MainActivity extends AppCompatActivity {

    private TextView tv;
    private EditText primo;
    private EditText secondo;
    //private ImageView iv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tv = findViewById(R.id.testo);
        //iv=findViewById(R.id.immagine);
        Button pulsante = findViewById(R.id.pulsante);
        primo = findViewById(R.id.primo);
        secondo = findViewById(R.id.secondo);

        /*
        tv.setText("Ciao");
        tv.setText(getString(R.string.prova));
        tv.setTextColor(Color.parseColor("#ff0000")); //#ff0000
        tv.setTextColor(ContextCompat.getColor(this,R.color.prova));

        iv.setImageDrawable(ContextCompat.getDrawable(this,R.drawable.ic_launcher_background));
        */
        pulsante.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View v) {
                int valore1=Integer.parseInt(primo.getText().toString());
                int valore2=Integer.parseInt(secondo.getText().toString());
                int somma=valore1+valore2;
                tv.setText(""+somma);
            }
        });
    }
}
