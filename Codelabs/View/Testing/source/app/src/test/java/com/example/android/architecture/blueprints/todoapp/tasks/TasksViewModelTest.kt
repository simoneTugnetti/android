package com.example.android.architecture.blueprints.todoapp.tasks

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.example.android.architecture.blueprints.todoapp.Event
import com.example.android.architecture.blueprints.todoapp.getOrAwaitValue
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.`is`
import org.hamcrest.Matchers.notNullValue
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class TasksViewModelTest {

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    private lateinit var tasksViewModel: TasksViewModel

    @Before
    fun setupViewModel() {
        tasksViewModel = TasksViewModel(ApplicationProvider.getApplicationContext())
    }

    @Test
    fun addNewTask_setsNewTaskEvent() {
        val observer = Observer<Event<Unit>> { }

        try {
            tasksViewModel.newTaskEvent.observeForever(observer)

            tasksViewModel.addNewTask()

            val value = tasksViewModel.newTaskEvent.value

            assertThat(value?.getContentIfNotHandled(), notNullValue())

        } finally {
            tasksViewModel.newTaskEvent.removeObserver(observer)
        }
    }

    @Test
    fun addNewTask_setsNewTaskEvent_Two() {
        tasksViewModel.addNewTask()

        val value = tasksViewModel.newTaskEvent.getOrAwaitValue()

        assertThat(value.getContentIfNotHandled(), notNullValue())

    }

    @Test
    fun setFilterAllTasks_tasksAddViewVisible() {
        val observer = Observer<Boolean> { }

        try {
            tasksViewModel.tasksAddViewVisible.observeForever(observer)

            tasksViewModel.setFiltering(TasksFilterType.ALL_TASKS)

            assertThat(tasksViewModel.tasksAddViewVisible.value ?: false, `is`(true))

        } finally {
            tasksViewModel.tasksAddViewVisible.removeObserver(observer)
        }

    }

}