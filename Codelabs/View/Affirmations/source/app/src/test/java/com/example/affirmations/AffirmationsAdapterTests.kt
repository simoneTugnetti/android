package com.example.affirmations

import android.content.Context
import com.example.affirmations.adapter.ItemAdapter
import com.example.affirmations.data.Datasource
import org.junit.Assert.assertEquals
import org.junit.Test
import org.mockito.Mockito.mock

class AffirmationsAdapterTests {

    private val context = mock(Context::class.java)

    @Test
    fun adapterSize() {
        val list = Datasource().loadAffirmations()

        val adapter = ItemAdapter(context, list)

        assertEquals("ItemAdapter is not the correct size", list.size, adapter.itemCount)

    }

}