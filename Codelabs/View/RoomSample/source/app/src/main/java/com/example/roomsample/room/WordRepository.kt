package com.example.roomsample.room

class WordRepository(private val wordDao: WordDao) {

    val allWords = wordDao.getAlphabetizedWords()

    fun insertWord(word: Word) = wordDao.insert(word)

}