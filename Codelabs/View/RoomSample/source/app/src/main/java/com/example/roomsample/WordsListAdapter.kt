package com.example.roomsample

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.roomsample.room.Word

class WordsListAdapter: ListAdapter<Word, WordsListAdapter.ViewHolder>(WordsComparator()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder.create(parent)

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bind(getItem(position).word)

    class WordsComparator: DiffUtil.ItemCallback<Word>() {
        override fun areItemsTheSame(oldItem: Word, newItem: Word) = oldItem == newItem
        override fun areContentsTheSame(oldItem: Word, newItem: Word) = oldItem.word == newItem.word
    }

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        private val title = itemView.findViewById<TextView>(R.id.textview)

        fun bind(title: String) {
            this.title.text = title
        }

        companion object {
            fun create(parent: ViewGroup) = ViewHolder(LayoutInflater.from(parent.context)
                .inflate(R.layout.recyclerview_item, parent, false))
        }

    }

}