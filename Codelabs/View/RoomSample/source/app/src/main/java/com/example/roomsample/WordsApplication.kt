package com.example.roomsample

import android.app.Application
import com.example.roomsample.room.WordRepository
import com.example.roomsample.room.WordRoomDatabase

class WordsApplication: Application() {

    // Using by lazy so the database and the repository are only created when they're needed
    // rather than when the application starts
    // No need to cancel this scope as it'll be torn down with the process
    val database by lazy { WordRoomDatabase.getDatabase(this) }
    val repository by lazy { WordRepository(database.wordDao()) }

}