package com.example.cupcake.model

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.map
import java.text.NumberFormat
import java.text.SimpleDateFormat
import java.util.*

class OrderViewModel: ViewModel() {

    private companion object {
        const val PRICE_PER_CUPCAKE = 2.00
        const val PRICE_PER_SAME_DAY_PICKUP = 3.00
    }

    private val _quantity = MutableLiveData(0)
    val quantity: LiveData<Int> = _quantity

    private val _flavor = MutableLiveData("")
    val flavor: LiveData<String> = _flavor

    private val _date = MutableLiveData("")
    val date: LiveData<String> = _date

    private val _price = MutableLiveData(0.0)
    val price: LiveData<String> = _price.map {
        NumberFormat.getCurrencyInstance().format(it)
    }

    val dateOptions = getPickupOptions()

    init {
        resetOrder()
    }

    fun setQuantity(numberCupcakes: Int) {
        _quantity.value = numberCupcakes
        updatePrice()
    }

    fun setFlavor(desiredFlavor: String) {
        _flavor.value = desiredFlavor
    }

    fun setDate(pickupDate: String) {
        _date.value = pickupDate
        updatePrice()
    }

    private fun updatePrice() {
        _price.value = (_quantity.value ?: 0) * PRICE_PER_CUPCAKE
        if (dateOptions.first() == _date.value)
            _price.value = _price.value?.plus(PRICE_PER_SAME_DAY_PICKUP)
    }

    fun hasNoFlavorSet() = _flavor.value.isNullOrEmpty()

    private fun getPickupOptions(): List<String> {
        val options = mutableListOf<String>()
        val formatter = SimpleDateFormat("E MMM d", Locale.getDefault())
        val calendar = Calendar.getInstance()
        repeat(4) {
            options.add(formatter.format(calendar.time))
            calendar.add(Calendar.DATE, 1)
        }
        return options
    }

    fun resetOrder() {
        _quantity.value = 0
        _flavor.value = ""
        _date.value = dateOptions.first()
        _price.value = 0.0
    }

}