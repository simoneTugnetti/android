package com.example.busschedule.viewmodels

import androidx.lifecycle.ViewModel
import com.example.busschedule.database.ScheduleDao

class BusScheduleViewModel(private val scheduleDao: ScheduleDao): ViewModel() {

    fun fullSchedule() = scheduleDao.getAll()

    fun scheduleForStopName(name: String) = scheduleDao.getByStopName(name)

}