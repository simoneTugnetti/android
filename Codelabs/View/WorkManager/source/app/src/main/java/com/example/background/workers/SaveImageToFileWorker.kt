package com.example.background.workers

import android.content.Context
import android.graphics.BitmapFactory
import android.provider.MediaStore
import android.util.Log
import androidx.core.net.toUri
import androidx.work.Worker
import androidx.work.WorkerParameters
import androidx.work.workDataOf
import com.example.background.KEY_IMAGE_URI
import com.example.background.OUTPUT_PATH
import java.io.File
import java.text.SimpleDateFormat
import java.util.*

class SaveImageToFileWorker(context: Context, param: WorkerParameters): Worker(context, param) {

    private companion object {
        const val TAG = "SaveImageToFileWorker"
    }

    private val title = "Blurred Image"
    private val dateFormatter = SimpleDateFormat("yyyy.MM.dd 'at' HH:mm:ss z",
        Locale.getDefault())

    override fun doWork(): Result {

        makeStatusNotification("Saving image", applicationContext)
        sleep()

        val resolver = applicationContext.contentResolver

        return try {
            val resourceUri = inputData.getString(KEY_IMAGE_URI) ?: ""
            val bitmap = BitmapFactory.decodeStream(resolver.openInputStream(resourceUri.toUri()))

            val imageUrl = MediaStore.Images.Media.insertImage(resolver, bitmap, title,
                dateFormatter.format(Date()))

            if (imageUrl.isNullOrEmpty()) {
                val output = workDataOf(KEY_IMAGE_URI to imageUrl)
                Result.success(output)
            } else {
                Log.e(TAG, "Writing to MediaStore failed")
                Result.failure()
            }

        } catch (e: Exception) {
            Result.failure()
        }

    }

}