package com.example.background.workers

import android.content.Context
import android.graphics.BitmapFactory
import android.util.Log
import androidx.core.net.toUri
import androidx.work.Worker
import androidx.work.WorkerParameters
import androidx.work.workDataOf
import com.example.background.KEY_IMAGE_URI
import com.example.background.R

class BlurWorker(context: Context, params: WorkerParameters): Worker(context, params) {

    private companion object {
        const val TAG = "BlurWorker"
    }

    override fun doWork(): Result {
        val appContext = applicationContext

        val resourceUri = inputData.getString(KEY_IMAGE_URI)

        makeStatusNotification("Thing's about to be blurry", appContext)

        sleep()

        return try {
            val picture = if (resourceUri.isNullOrEmpty())
                BitmapFactory.decodeResource(appContext.resources, R.drawable.android_cupcake)
            else BitmapFactory.decodeStream(appContext.contentResolver.openInputStream(resourceUri.toUri()))

            val pictureUri = blurBitmap(picture, appContext).run {
                writeBitmapToFile(appContext, this)
            }

            Result.success(workDataOf(KEY_IMAGE_URI to pictureUri.toString()))
        } catch (e: Throwable) {
            Log.e(TAG, "Error applying blur")
            Result.failure()
        }

    }

}