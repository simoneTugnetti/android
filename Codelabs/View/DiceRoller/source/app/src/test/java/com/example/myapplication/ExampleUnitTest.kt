package com.example.myapplication

import com.example.diceroller.Dice
import org.junit.Assert.assertTrue
import org.junit.Test

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {

    @Test
    fun generateNumber() {
        val result = Dice(10).roll()
        assertTrue("The value of rollResult was not between 1 and 6",
            result in 1..6)
    }

}