# Suggestions Cover

Questo progetto è stato creato durante il mio periodo di stage compreso 
all'interno della formazione scolastica per eseguire il suggerimento di 
immagini di copertina di libri attraverso una piattaforma specifica.

Qui elecante alcune delle sue funzionalità:
- Android Jetpack
- ViewModel e LiveData
- DataBinding
- RecyclerView
- Retrofit
- Glide
- Dexter
- Google Play Services Vision
- Room
- Firebase Firestore e Storage

Questa applicazione è puramente a scopo DESCRITTIVO.

Per ottenere un'interagibilità con essa bisogna avere due parti essenziali:
- File google-service.json da inserire nella cartella "app", ottenibile creando un progetto su Firebase avente abilitato Cloud Firestore e Storage
- Goodreads API key da inserire in "api_key.xml", ottenibile dall'ononimo sito